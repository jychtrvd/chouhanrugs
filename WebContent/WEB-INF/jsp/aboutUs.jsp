<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- head -->
<%@include file="head.jsp"%>
<!-- End head -->
<!--JS-->
<%@include file="mainJs.jsp"%>
<!-- End JS -->
<body>
	<!--header-->
	<%@include file="header.jsp"%>
	<!-- End header -->

	<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>
				<span> </span>
			</h3>
			<!--/w3_short-->
			<div class="services-breadcrumb">
				<div class="agile_inner_breadcrumb">

					<ul class="w3_short">
					</ul>
				</div>
			</div>
			<!--//w3_short-->
		</div>
	</div>
	<!-- /banner_bottom_agile_info -->
	<div class="banner_bottom_agile_info"
		style="margin-top: 80px; margin-bottom: 80px;">


		<div class="container">
			<div class="agile_ab_w3ls_info">

				
				<c:if test="${company_visible == 0}">

					<div class="col-md-6 ab_pic_w3ls">
						<img src="${pageContext.request.contextPath}/images/logo.png" alt=" " class="img-responsive"
							style="height: 227px; width: 539px;" />
					</div>
					<div class="col-md-6 ab_pic_w3ls_text_info">
						<h2>
							About Our ${company_name} <span> Shop</span>
						</h2>
						<br>


						<p>India has its own enviable style in textiles and here at
						Kriti fab are proud to be part of that exciting industry. Kriti
						fab specializes in Hand Block Printing,Screen Printing a craft
						which is in the fore front of the fashion scene today.</p>
						<br>

						<p>Our
						manufacturing unit is based in Sanganer where we present to our
						clients the colourful and intrinsic crafts of India, Our in-house
						team of designers work round the year in designing new innovative
						styles, patterns and products, according to fashion and market
						trends in the Home textiles area as well as up to date ready to
						wear fashion.</p>
						
						<br>

						<p>${company_director_name} (Founder and CEO ${company_name})</p>
						<br>






					</div>

				</c:if>


				<div class="clearfix"></div>
			</div>

		</div>
	</div>


	<!--footer-->
	<%@include file="footer.jsp"%>
	<!-- End footer -->

	<a href="#" id="toTop">To Top</a>

</body>
</html>