<div class="header">
	<div class="top-header" style="padding: 1em 0px 1em;">
		<div class="container">
			<div class="top-head">
				<div class="header-para">
					<ul class="social-in">
						<li><a href="#"><i> </i></a></li>
						<li><a
							href="${company_facebook}"><i
								class="ic"> </i></a></li>
						<li><a href="${company_instagram}"><i class="ic1"> </i></a></li>
					</ul>
				</div>
				<ul class="header-in">
				<%--	<li><a href="${pageContext.request.contextPath}/products">
							Brands</a></li> --%>
					<li><a href="${pageContext.request.contextPath}/">
							Home</a></li>
					<li><a href="${pageContext.request.contextPath}/aboutUs">About
							us</a></li>
					<li><a href="${pageContext.request.contextPath}/contactUs">
							Contact us</a></li>

					<c:if test="${(sessionScope.loginSession != null)}">
						<li><a href="${pageContext.request.contextPath}/buyer/">${sessionScope.loginSession.name}</a></li>
					</c:if>

	                <c:if test="${sessionScope.loginSession == null}">
						<li><a href="${pageContext.request.contextPath}/login?shop=log">Login & Signup</a></li>
					</c:if>
				</ul>
				<div class="search-top">
					<div class="search">
						<form method="get" action="products">
							<input type="text" name="q" placeholder="search about something?"
								onfocus="this.value = '';"
								onblur="if (this.value == '') {this.value = 'search about something ?';}" required>
							<input type="submit" value="">
						</form>
					</div>
					<div class="world">
						<ul>
							<li><a href="#"><span> </span></a></li>
							<li><select class="in-drop">
									<option>EN</option>
							</select></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!---->
	<div class="header-top"
		style="padding-top: 12px; padding-bottom: 12px;">
		<div class="container">
			<div class="head-top">
			<a href="${pageContext.request.contextPath}/">
				<div class="logo">
					<img
						class="img-responsive" src="${pageContext.request.contextPath}/images/logo.png" alt=""
						style="height: 43px; margin-left: -10px; margin-bottom: -68px; margin-top: -11px;">
				</div></a>
				<%@include file="menus.jsp"%>
				<div class="cart box_1" id="cartBox">
					<h3>
						<div class="total">
							<a href="${pageContext.request.contextPath}/cart"
								class="simpleCart_empty">${currency_dollar}<span
								class="simpleCart_total1"></span> <%--  (<span id="simpleCart_quantity" class="simpleCart_quantity1"></span> items)--%>
						</div>
						<img src="${pageContext.request.contextPath}/images/cart.png" alt="">
					</h3>
					</a>
					<%-- <p><a href="cart" class="simpleCart_empty"><img src="images/cart-c.png" alt=""></a></p>  --%>
					<div class="clearfix"></div>


				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>