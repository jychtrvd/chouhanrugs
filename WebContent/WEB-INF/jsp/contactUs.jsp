<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<!-- head -->
<%@include file="head.jsp"%>
<!-- End head -->
<style type="text/css">
label.error {
	font-size: 11px;
	background-color: #cc0000;
	color: #FFFFFF;
	padding: 3px;
	margin-left: 5px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
}
</style>
<!--JS-->
<%@include file="mainJs.jsp"%>
<!-- End JS -->
<body>
	<!--header-->
	<%@include file="header.jsp"%>
	<!-- End header -->
	<div class="container">
		<div class="contact">
			<h2>CONTACT US...</h2>
			<div class="contact-in">
				<div class=" col-md-3 contact-right">
					<h5>${company_name}</h5>
						${company_address }
					<p>Phone:${company_contact}</p>
					<p>Fax: (000) 000 00 00 0</p>
					<p>
						Email: <a href="mailto:${company_email}">${company_email}</a>
					</p>
					<p>
						Follow on: <a href="${company_facebook}">Facebook</a>, <a href="${company_twitter}">Twitter</a>
					</p>

					<p>Monday-Friday</p>
					<p>
						10:00 am - 5.00 pm IST (Indian Standard Time)<br>
					</p>
					<p>Note:- Closed on bank holidays</p>
				</div>





				<div class=" col-md-9 contact-left">


					${error}

					<form id="queryForm" method="post">
						<div class="col-sm-12">
							<div>
								<span>Name</span> <input name="name" id="name" type="text"
									placeholder="Enter Name Here.." class="form-control">
							</div>
							<div>
								<span>Email</span> <input name="username" id="username"
									type="text" placeholder="Enter Email Address Here.."
									class="form-control">
							</div>
							<div>
								<span>Subject</span>
								<textarea name="userMsg" id="userMsg" type="text"
									placeholder="Type Your Message" class="form-control"> </textarea>
							</div>
							<!-- /.col -->
							<div>
								<br>
								<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
							</div>
							<!-- /.col -->
						</div>
				</div>
				</form>





			</div>


			<div class="clearfix"></div>
		</div>

	</div>
	</div>
	<div class="map">
		${company_map}
	</div>



	<!--footer-->
	<%@include file="footer.jsp"%>
	<!-- End footer -->

	<a href="#" id="toTop">To Top</a>

	<!--JS-->


	<!-- End JS -->
	<script>
		$(function() {
			jQuery.validator.addMethod("lettersonly", function(value, element) {
				return this.optional(element) || /[a-zA-Z]+$/i.test(value);
			}, "Letters only please with no space");
			jQuery.validator.addMethod("noSpace", function(value, element) {
				return value.indexOf(" ") < 0 && value != "";
			}, "No space please and don't leave it empty");
			var ruleCall = {
				name : "required",
				name : {
					required : true,
					minlength : 3,
					noSpace : false
				},
				username : {
					required : true,
					minlength : 5,
					lettersonly : true,
					noSpace : true
				},
				userMsg : {
					required : true,
					minlength : 10,
					noSpace : false
				}
			};
			var msg = {
				name : "Enter your name",
				username : "Enter username or email id",
				pswd : "Please type your message minimum 10 word."
			};
			validatorAjax("#queryForm", ruleCall, msg, 0);
		});
		function submitForm(form, id) {
			form.submit();
		}
	</script>
</body>
</html>