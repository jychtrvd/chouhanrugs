<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<!-- head -->
	<%@include file="head.jsp"%>
<!-- End head -->
<style>
.wrap {
    width: 1060px;
    height:auto;
    margin: auto;
    text-align:center;
    position:relative;
}
.text_over_image {
    position: absolute;
    margin: auto;
    top: 0;
    left: 80px;
    right:0;
    bottom:-220px;
    color:#09252f;
    height:20px;
}


</style>
<!--JS-->	
	<%@include file="mainJs.jsp"%>
<!-- End JS -->
<body> 
<!--header-->	
	<%@include file="header.jsp"%>
<!-- End header -->

<section class="content">
        <div class="container-fluid">
            <!-- Image Gallery -->
            <div class="block-header">
                
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-1.jpg" style=" height: 279px;width: 282px;">
                                 		<h3 class="text_over_image"> Rugs Pillow - 1</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-2.jpg" style=" height: 279px;width: 282px;">
                                 		<h3 class="text_over_image">Rugs Pillow - 2</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-3.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Rugs Pillow - 3</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-4.jpg" style=" height: 279px;width: 282px;">
                               			<h3 class="text_over_image">Rugs Pillow - 4</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-5.jpg" style=" height: 279px;width: 282px;">
                               			<h3 class="text_over_image">Floor Jute - 1</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-6.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Floor Jute - 2</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-7.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Floor Jute - 3</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-8.jpg" style=" height: 279px;width: 282px;">
                                 		<h3 class="text_over_image">Floor Kaleen</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-9.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 1</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-10.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 2</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-11.jpg" style=" height: 279px;width: 282px;">
                                   		<h3 class="text_over_image">Sample Image - 3</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-12.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 4</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                   <!--   <a href="images/image-gallery/13.jpg" data-sub-html="Demo Description"> </a>  -->
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-13.jpg" style=" height: 279px;width: 282px;">
                                    	<h3 class="text_over_image">Sample Image - 5</h3>
                                    
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-14.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Sample Image - 6</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-15.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 7</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-16.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Sample Image - 8</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-17.jpg" style=" height: 279px;width: 282px;">
                                		<h3 class="text_over_image">Sample Image - 9</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-18.jpg" style=" height: 279px;width: 282px;">
                                 		<h3 class="text_over_image">Sample Image - 10</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-19.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 11</h3>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <img class="img-responsive thumbnail" src="images/image-gallery/thumb/thumb-20.jpg" style=" height: 279px;width: 282px;">
                                  		<h3 class="text_over_image">Sample Image - 12</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	
	<!--footer-->	
	<%@include file="footer.jsp"%>
<!-- End footer -->
	
<a href="#" id="toTop">To Top</a>
<script src="${pageContext.request.contextPath}/js/pages/medias/image-gallery.js?randId=${rand}"></script>
<script src="${pageContext.request.contextPath}/plugins/light-gallery/js/lightgallery-all.js"></script>
</body>
</html>