<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- head -->
<%@include file="head.jsp"%>
<!-- End head -->
<!--JS-->
<%@include file="mainJs.jsp"%>
<!-- End JS -->
<body>
	<!--header-->
	<%@include file="header.jsp"%>
	<!-- End header -->

	<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>
				<span> </span>
			</h3>
			<!--/w3_short-->
			<div class="services-breadcrumb">
				<div class="agile_inner_breadcrumb">

					<ul class="w3_short">
					</ul>
				</div>
			</div>
			<!--//w3_short-->
		</div>
	</div>
	<!-- /banner_bottom_agile_info -->
	<div class="banner_bottom_agile_info"
		style="margin-top: 80px; margin-bottom: 80px;">


		<div class="container">
			<div class="agile_ab_w3ls_info">

				
				<c:if test="${company_visible == 0}">

				<div class=" col-md-12 contact-right">
						<h2>
							Return Policies
						</h2>
						<br>
					<p><h4>Exchange or refund will be done in the following cases:</h4></p>
					
					<p>
					Genuine sizing issues, only for unused garments. Please review our size guides carefully
 					before making your purchase. Genuine quality issues. Packages lost in transit. In case a
 					wrong item has been shipped to you.</p>
					<br>
					<p><h4>No refund or exchange will be given in the following cases :</h4></p>

					<p>Products returned in a used or damaged condition.</p>
					<br>
					<p><h4>How to Returns:</h4></p>

					<p>In the unlikely event that you receive a defective product and want to return it, you may 
					use a courier service available in your city if our authorized courier company can’t arrange
 					pickup. We will bear the courier charge.</p>

					<p>Returns, if any, must be made within 15 days of receiving the goods. A Return & Exchange 
					Form should be completed and sent along with the goods. You can download the Return & Exchange
 					Form by clicking here. Returns and exchanges can only happen once we receive the returned 
					product from the courier service. In case of exchange, customer will bear courier charges. 
					Cash Refunds are not allowed for COD orders since we cannot get to deliver cash through courier, 
					hence refund will be made through cheque payable at par.</p>
					<br>
					<p><h4>No Returns, no exchanges for discounted or promotional items.</h4></p>
					<br>
					<p>For returns and exchanges, please ship to the following address:</p>

					
					<h5>${company_name}</h5>
						${company_address }
					<p>Phone:${company_contact}</p>
					<p>
						Email: <a href="mailto:${company_email}">${company_email}</a>
					</p>
					<p>All disputes subject to Jaipur Jurisdiction  - Rajasthan - India.</p>
				</div>
					





				</c:if>


				<div class="clearfix"></div>
			</div>

		</div>
	</div>


	<!--footer-->
	<%@include file="footer.jsp"%>
	<!-- End footer -->

	<a href="#" id="toTop">To Top</a>

</body>
</html>