<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<!-- head -->
<%@include file="head.jsp"%>
<style type="text/css">
label.error {
	font-size: 11px;
	background-color: #cc0000;
	color: #FFFFFF;
	padding: 3px;
	margin-left: 5px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
}
</style>
<!-- End head -->
<body>
	<!--header-->
	<%@include file="header.jsp"%>
	<!-- End header -->
	<div class="container">








		<div class="col-lg-12 well">
			<h1 class="well text-center">Add Shipping Address</h1>
			<div class="row">
			
			<c:if test="${reginfo ne null}">
			<div class="col-sm-3">
					<address>
						<strong>${reginfo.name}</strong><br> ${reginfo.street}<br>
						${reginfo.address}<br> ${reginfo.city}<br>
						${reginfo.state}-${reginfo.pincode}<br> <abbr title="Phone">P:</abbr>
						+91-${reginfo.mobile}
					</address>
					<div class="row">
					<button type="button" class="btn btn-primary btn-block btn-flat" onclick="updateShip('regShip');">Ship Same Address</button>
					<br>
					</div>
					<div class="row">
					<button type="button" class="btn btn-primary btn-block btn-flat" onclick="addShipForm();">Add Shipping Address</button>
					</div>
			
			</div>
			
				<c:forEach items="${error}" var="msg">
					<p>${msg}</p>
				</c:forEach>
				<form id="reg" method="post" Style="display: none;" name="reg">
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label> Shipping Name</label> <input name="sName"
									value="${empty sessionScope.ship.sName?'':sessionScope.ship.sName}"
									type="text" placeholder="Enter Shipping Name Here.."
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>Street/plot no.:'</label> <input name="sStreet"
									value="${empty sessionScope.ship.sAddress1?'':sessionScope.ship.sAddress1}"
									type="text" placeholder="Enter plot number Here.."
									class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Shipping Address</label> <input name="sAddress"
									value="${empty sessionScope.ship.sAddress2?'':sessionScope.ship.sAddress2}"
									type="text" placeholder="Enter Shipping Address Here.."
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>Contact Number</label> <input name="sMobile"
									value="${empty sessionScope.ship.sMobile?'':sessionScope.ship.sMobile}"
									type="text" placeholder="Enter Contact Number Here.."
									class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label> State </label> <input name="sState"
									value="${empty sessionScope.ship.sState?'':sessionScope.ship.sState}"
									type="text" placeholder="Enter State" class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label> City </label> <input name="sCity"
									value="${empty sessionScope.ship.sCity?'':sessionScope.ship.sCity}"
									type="text" placeholder="Enter City" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>PIN Number</label> <input name="sPincode"
									value="${empty sessionScope.ship.sPincode?'':sessionScope.ship.sPincode}"
									type="text" placeholder="Enter PIN No." class="form-control">
							</div>
							
						</div>


						<!-- /.col -->
						
							<button type="submit" class="btn btn-primary btn-block btn-flat">Checkout</button>
						
					</div>
				</form>
				</c:if>
				<c:if test="${reginfo eq null}">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
				
				<h3>
					Hi,<strong>${reginfo.name}</strong>
				</h3>
				<br>

				<h5>its, mendatory to fill below information for checkout or
					delievery!</h5>
				<br>

				<form name="personal" method="post">
					<div class="form-group">
						<label for="street">Street/plot no.:</label> <input type="street"
							class="form-control" name="street" id="street">
					</div>
					<div class="form-group">
						<label for="address">Address:</label> <input type="address"
							class="form-control" name="address" id="address">
					</div>
					
					<div class="form-group">
						<label for="state">State:</label> <input type="address"
							class="form-control" name="state" id="state">
					</div>
					
					<div class="form-group">
						<label for="city">City:</label> <input type="address"
							class="form-control" name="city" id="city">
					</div>
					
					<div class="form-group">
						<label for="pincode">Pincode:</label> <input type="address"
							class="form-control" name="pincode" id="pincode">
					</div>
					
					
					<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
				</form>
				
				</div>
				<div class="col-sm-3"></div>
				

				</c:if>
				
			</div>
		</div>
	</div>
	<!--footer-->
	<%@include file="footer.jsp"%>
	<!-- End footer -->
	<a href="#" id="toTop">To Top</a>
	<!--JS-->
	<%@include file="mainJs.jsp"%>
	<!-- End JS -->
	<script>
	$(function() {
		jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /[a-zA-Z]+$/i.test(value);
		}, "Letters only please with no space");
		jQuery.validator.addMethod("noSpace", function(value, element) {
			return value.indexOf(" ") < 0 && value != "";
		}, "No space please and don't leave it empty");
		
		
		var ruleCall2 = { street : { required : true, minlength : 3 },
				address : { required : true, minlength : 3 }, city : { required : true, minlength : 3, lettersonly : true, noSpace : true }, 
				state : { required : true, minlength : 3, lettersonly : true, noSpace : true }, pincode : 
		{ required : true, minlength : 6, maxlength : 6, number : true }, 
		};
		var msg2 = { street : { required : '*invalid street name.' }, address : { required : '*invalid address name.' },
				pincode : { required : '*invalid pincode name.' }, state : { required : '*invalid state name.' }  
				};
		
		
		var ruleCall ={ sStreet : "required",sAddress: "required",
				sName : { required : true, lettersonly : true}, 
				pswd : { required : true, minlength : 5 }, 
				cpswd : { required : true, minlength : 5, equalTo : "#pswd" }, 
				sMobile : { required : true, minlength : 10, maxlength : 10, number : true },
				mobile : { required : true, minlength : 10, maxlength : 10, number : true },
				sState : { required : true, lettersonly : true}, 
				sCity : { required : true, lettersonly : true},
				sPincode : { required : true, minlength : 6, maxlength : 6, number : true },};
		var msg ={
				sName : "Please enter Shipping Name", 
				sStreet : "Enter Street Address!",
				sAddress : "Enter Shipping Address",  
				sMobile : { required : "Please enter shipping contact", minlength : " contact must be at least 10 digit", maxlength : " contact must be at least 10 digit", },
				sPincode : { required : "Please provide a your mobile number" },
				
				sState : { required : "Please Enter State" }, 
				sCity : { required : "Please Enter your city" }, 
				sPincode : { required : "Please provide a your PIN No." }, }; 
		
		validatorAjaxMultiple("form[name='personal']", ruleCall2, msg2, 0,requestHandler2);
		validatorAjaxMultiple("#reg", ruleCall, msg, 0,requestHandler2);
	
	

		$('#simpleCart_quantity').text('0');
		$('#simpleCart_total').text('0');
		loadCart();
		

	  }
	
	
	
	
	);
	
	
	function requestHandler2(res) {
		var formname=$(res)[0].name;
		if(formname=='personal')
			{
			updateShip('updateReg');
			}
		if(formname=='reg')
		{
			form.submit();
		}
		
	}
	
	
		function submitForm(form,id){
			alert('ID= '+id);
			//if(id=='form[name='personal']')
			//	{
			//	alert('ID  22 = '+id);
				//updateShip('updateReg');
			//	}
			//if(id=="reg")
			//{
			
			//}
				//  form.submit();
		}
		function addShipForm() {
			$("form[name='reg']").show();
		}
		function updateShip(cas) {
			var js1 = new Object();
			js1.cas = cas;
			if(cas=='updateReg')
				{
				js1.street = $('#street').val().trim();
				js1.address = $('#address').val().trim();
				js1.state = $('#state').val().trim();
				js1.city = $('#city').val().trim();
				js1.pincode = $('#pincode').val().trim();
				}
			
			var json = {
				'json' : JSON.stringify(js1)
			};
			ajaxRequest(json, 'get', '/updateShipp', div3);

		}
		function div3(response) {
			var obj = JSON.stringify(response);
			var par = JSON.parse(obj);
			if (par.res == true) {
				if(par.url == 'updateReg') {
					location.reload();
				} 
				if(par.url == 'regShip') {
						window.location.href='payment';
					}
				
			}
		}
	</script>
</body>
</html>