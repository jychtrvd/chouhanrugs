<%@include file="buyerTopHead.jsp"%>
</head>
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header">
		</div>
		<div class="row clearfix">
				<div class="row">
					<div class="col-xs-12">&#8201;
					
					<div class="panel panel-default">
							<span class="label label-default">Order Information</span>
							<div class="table table-bordered table-responsive table-hover">
								<div id="tab1" style="display: block;">
									<table class="table table-bordered table-responsive table-hover">
										<tbody>
											<tr>
												<th>Product Image</th>
												<th>Product name</th>
												<th>Product Key</th>
												<th>Price</th>
												<th>Selling Price</th>
												<th>Quantity</th>
												<th>Shipping Charge</th>
												<th>Total</th>
											</tr>
											<c:forEach items="${itemDetail}" var="num">
												<tr>
													<td class="image-column"><a target="_blank"
														href="${pageContext.request.contextPath}/single?pkey=${num.pkey}">
															<img
															src="${pageContext.request.contextPath}/temp/img/eVY135tpf6.jpg"
															width="20px" height="20px" alt="juice">
													</a></td>
													<td>${num.title}</td>
													<td>${num.pkey}</td>
													<td>${num.mrp}</td>
													<td>${num.smrp}</td>
													<td>${num.qty}</td>
													<c:set var="sValue"
															value="${sValue=sValue+1}" />
													
													<c:set var="totalValue"
														value="${totalValue=totalValue+num.smrp*num.qty}" />
												<c:if test="${totalValue gt free}">
												<td>${currency_dollar}0</td>
													<td>${num.smrp*num.qty}</td>
												</c:if>
												<c:if test="${totalValue eq free}">
												<td>${currency_dollar}0</td>
													<td><strong>${currency_dollar}Free shipping</strong></td>
												</c:if>
												<c:if test="${totalValue le free}">
												<td>${currency_dollar}${courier}</td>
												<td>${num.smrp*num.qty+courier*num.qty}</td>
												</c:if>
												</tr>
											</c:forEach>

											<tr>
												<td class="align-right" colspan="4"><span
													class="price big">Total</span></td>
												<c:if test="${totalValue gt free}">
													<td><strong>${currency_dollar}${totalValue}</strong></td>
													<td><strong>${currency_dollar}Free shipping</strong></td>
													<td>${currency_dollar}${totalValue}</td>
												</c:if>
												<c:if test="${totalValue eq free}">
													<td><strong>${currency_dollar}${totalValue}</strong></td>
													<td><strong>${currency_dollar}Free shipping</strong></td>
													<td>${currency_dollar}${totalValue}</td>
												</c:if>
												<c:if test="${totalValue le free}">
													<td><strong>${currency_dollar}${totalValue}</strong></td>
													<td><strong>${currency_dollar}${sValue*courier}</strong></td>
													<td>${currency_dollar}${totalValue+sValue*courier}</td>
												</c:if>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				
				
				<div class="row">

				<div class="col-xs-12">
					&#8201;

					<div class="panel panel-default">
						<span class="label label-default">Dispatch Courior Information</span>
						<c:if test="${empty couriorDetail or couriorDetail eq null}">
					<div class="table table-bordered table-responsive table-hover">
						<div class="alert alert-danger" role="alert">
							  Still Dispatch is not confirm!
						</div>
						
						</div>
						</c:if>
						<c:if test="${not empty couriorDetail or couriorDetail eq null}">
							<c:forEach items="${couriorDetail}" var="innerList2">
							<table class="table table-bordered table-hover">
								<tbody>
									<tr>
										<th>company name</th>
										<td>${innerList2[0]} </td>
									</tr>
									<tr>
										<th>tracking id</th>
										<td>
										${innerList2[1]}</td>
									</tr>
								</tbody>
							</table>
                               </c:forEach>
						</c:if>
						
					</div>

				</div>
				<!-- /.row -->
			</div>
				
				
				
				
				
				<div class="row">
					
					<div class="col-xs-12">&#8201;
					
					<div class="panel panel-default">
						<span class="label label-default">Payment Information</span>
						<div class="table table-bordered table-responsive table-hover">
						<c:forEach items="${orderDetail}" var="innerList">
							<table class="table table-bordered table-hover">
								<tbody>
									<tr>
										<th>Order number</th>
										<td>${innerList[0]} </td>
									</tr>

									<tr>
										<th>Order date</th>
										<td>
										<fmt:formatDate type="both" dateStyle="medium"
												timeStyle="medium" value="${innerList[2]}" /></td>
									</tr>

									<tr>
										<th>Payment status</th>
										<td> 
										 <c:choose>
											<c:when test="${innerList[4] eq 1}">
											       Confirmed
											    </c:when>
												<c:otherwise>
												    pending
												    </c:otherwise>
											</c:choose>
											</td>
									</tr>
									<tr>
										<th>Shipment</th>
										<td>
										 <c:choose>
												 <c:when test="${innerList[4] eq -1}">
													payment not successful
											    </c:when>
											<c:when test="${innerList[4] eq 1}">
											       <c:if test="${innerList[3] eq 0  }">
						                                pending from admin confirmation
											       </c:if>
											        <c:if test="${innerList[3] eq 1}">
											       		Order confirm
											       </c:if>
											        <c:if test="${innerList[3] eq -1}">
											       		Order cancel by admin
											       </c:if>
											    </c:when>
											   <c:when test="${innerList[4] eq 0}">
											        <c:if test="${innerList[3] eq 0}">
						                                pending from admin confirmation due payment
											       </c:if>
											        <c:if test="${innerList[3] eq -1}">
											        	Order Cancel due to payment not success
											       </c:if>
											       
											    </c:when>
												<c:otherwise>
												    -----
												    </c:otherwise>
											</c:choose>
										
										</td>
									</tr>
									<tr>
										<th>Total</th>
										<td><span class="price">${innerList[1]}</span></td>
									</tr>
								</tbody>
							</table>
</c:forEach>




						</div>
					</div>
						
					</div>
					<!-- /.row -->
				</div>
				<div class="row">
					
					<div class="col-xs-12">&#8201;
					
					<div class="panel panel-default">
							
							<div class="table table-bordered table-responsive table-hover">
								
								<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="carousel-heading">
							<h4>Bill to</h4>
						</div>
						<table class="orderinfo-table table-responsive">
							<tbody>
								<tr>
									<th>Name</th>
									<td>${regInfo.name}</td>
								</tr>
								<tr>
									<th>Address 1</th>
									<td>${regInfo.street}</td>
								</tr>
								<tr>
									<th>Address 2</th>
									<td>${regInfo.address}</td>
								</tr>
								<tr>
									<th>ZIP / Postal code</th>
									<td>${regInfo.pincode}</td>
								</tr>

								<tr>
									<th>City</th>
									<td>${regInfo.city}</td>
								</tr>
								<tr>
									<th>State</th>
									<td>${regInfo.state}</td>
								</tr>
								<tr>
								<tr>
									<th>Country</th>
									<td>India</td>
								</tr>
								<tr>
									<th>Phone</th>
									<td>+91-${regInfo.mobile}</td>
								</tr>
							</tbody>
						</table>

					</div>
					
					<div class="col-lg-6 col-md-6 col-sm-6">

						<div class="carousel-heading">
							<h4>Ship to</h4>
						</div>

						<table class="orderinfo-table">
							<tbody>
								<tr>
									<th>Name</th>
									<td>${shippingAdd.sName}</td>
								</tr>
								<tr>
									<th>Address 1</th>
									<td>${shippingAdd.sAddress1}</td>
								</tr>
								<tr>
									<th>Address 2</th>
									<td>${shippingAdd.sAddress2}</td>
								</tr>
								<tr>
									<th>ZIP / Postal code</th>
									<td>${shippingAdd.sPincode}</td>
								</tr>

								<tr>
									<th>City</th>
									<td>${shippingAdd.sCity}</td>
								</tr>
								<tr>
									<th>State</th>
									<td>${shippingAdd.sState}</td>
								</tr>
								<tr>
								<tr>
									<th>Country</th>
									<td>India</td>
								</tr>
								<tr>
									<th>Phone</th>
									<td>+91-${shippingAdd.sMobile}</td>
								</tr>
							</tbody>
						</table>
					</div>
								
								
							</div>
						</div>
						
					</div>
					<!-- /.row -->
				</div>
		</div>
	</div>
	</section>
	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
<link
	href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/dt/css/buttons.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	

<script
	src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
	type="text/javascript"></script>

<script
	src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
	type="text/javascript"></script>	

<script type="text/javascript">


		</script>
</body>
</html>