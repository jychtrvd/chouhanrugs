<%@include file="buyerTopHead.jsp"%>
</head>

<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header">
		</div>
		<div class="row clearfix">
			<!-- Visitors -->
				
		</div>

		<div class="row clearfix">
			<!-- Task Info -->
			
			<!-- #END# Task Info -->
			<!-- Browser Usage -->

			<!-- #END# Browser Usage -->
		</div>
	</div>
	</section>

	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
<script type="text/javascript">
		$(document).ready(function() {
			
			getContent(1);
			$('#addId').hide();
			$('input[type="file"]').ajaxfileupload({
		   	      'action': '<%=request.getContextPath()%>/UploadFile?cate=single',	
		   	      'type': 'post', 
		   	  'onComplete': function(response) {	        
		   	        $('#upload').hide();
		   	        $('#imgivew').empty();
		   	         $.each($.parseJSON(response.url), function (index, value) {
		   	        	 console.log('**1***   '+value);
		   	        	 value=value.match(/[-_\w]+[.][\w]+$/i)[0];
		   	        	console.log('**2***   '+value);
		   	        	updateImg('updateImage',value);
		   	                $('#imgivew').prepend('<img width="100px" height="50px" alt="" class="img-circle img-responsive"  src="../temp/img/'+value+'" />');
		   	            });
		   	        
		   	      },
		   	      'onStart': function() {
		   	        $('#upload').show(); 
		   	       
		   	      }
		   	 });
			 
		});
		
		 $('#clickUpdate').on('click', function (e) {
		    	alert('sdfdsf');
		    	var email="input[name='email']" ;
				var isDisabled = $('#loginField').is(':disabled');
				   if (isDisabled) {
					   alert('hello1');
				   //	fieldsetEnableDisable(email,true,'#loginField');
				   	$('#loginField').attr("disabled",false);
				    } else {
				    	alert('hello2');
			    	//fieldsetEnableDisable(email,false,'#loginField');
			    	    updateEmail();
				    	$('#loginField').attr("disabled",true);
				    }
		    });
		

		function addInfo() {
			$('#addId').show();
			
			}


		function updateImg(cas,url) {
			console.log('url '+url);
			var js1 = new Object();
			js1.cas = cas;
			js1.url = url;
					var json = {
							'json' : JSON.stringify(js1)
						};
				//	ajaxRequest(json, 'get','/updateShipp', div);
					ajaxCallRequestResponse('json',json,'get','../updateShipp',div3);
		}
		function div(response) {
			var obj = JSON.stringify(response);
			var par = JSON.parse(obj);
			if (par != null) {
					$('#errorForm').append("<span class=\"label label-success\">Success!</span>");
					$('#addId').hide();
				}

			else {
				$('#errorForm').append("<span class=\"label label-info\">please try again!</span>");
			}
			

		}
	</script>
	
<link
	href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/dt/css/buttons.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	

<script
	src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
	type="text/javascript"></script>

<script
	src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
	type="text/javascript"></script>	



</body>
</html>