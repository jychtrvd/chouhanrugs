

<nav class="navbar">
            <div class="navbar-header">
                
                <a href="javascript:void(0);" class="bars" style="display: none;"></a>
               
			 </div>	
          
    
    <div class="header">
	<div class="top-header" style="padding: 1em 0px 1em;">
		<div class="container">
			<div class="top-head">
				<div class="header-para">
					<ul class="social-in">
						<li><a href="#"><i> </i></a></li>
						<li><a
							href="${company_facebook }"><i
								class="ic"> </i></a></li>
						<li><a href="#"><i class="ic1"> </i></a></li>
					</ul>
				</div>
				<ul class="header-in">
					<li><a href="${pageContext.request.contextPath}/products">
							Brands</a></li>
					<li><a href="${pageContext.request.contextPath}/aboutUs">About
							us</a></li>
					<li><a href="${pageContext.request.contextPath}/contactUs">
							Contact us</a></li>

					<c:if test="${(sessionScope.loginSession != null)}">
						<li><a href="${pageContext.request.contextPath}/buyer/">${sessionScope.loginSession.name}</a></li>
					</c:if>

	                <c:if test="${sessionScope.loginSession == null}">
						<li><a href="${pageContext.request.contextPath}/login">Login & Signup</a></li>
					</c:if>
				</ul>
				<div class="search-top">
					<div class="search">
						<form method="get" action="products">
							<input type="text" name="q" placeholder="search about something?"
								onfocus="this.value = '';"
								onblur="if (this.value == '') {this.value = 'search about something ?';}" required>
							<input type="submit" value="">
						</form>
					</div>
					<div class="world">
						<ul>
							<li><a href="#"><span> </span></a></li>
							<li><select class="in-drop">
									<option>EN</option>
							</select></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!---->
	
</div>
           
</nav>