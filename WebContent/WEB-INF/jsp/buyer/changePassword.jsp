<%@include file="buyerTopHead.jsp"%>
</head>
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header">
		</div>
		<div class="row clearfix">
				<div class="row">
					<div class="col-xs-12">&#8201;
						<form>
							<div class="form-group">
								<label for="oldInputPasswd">Old Password</label> <input
									type="password" class="form-control" id="oldInputPasswd"
									placeholder="Old Password">
							</div>
							<div class="form-group">
								<label for="newInputPasswd">New Password</label> <input
									type="password" class="form-control" id="newInputPasswd"
									placeholder="New Password">
							</div>
							<div class="form-group">
								<label for="retypeInputPasswd">Re-type New Password</label> <input
									type="password" class="form-control" id="retypeInputPasswd"
									placeholder="Re-type New Password">
							</div>
							<button type="submit" onclick="changePwd();"
								class="btn btn-default btn-theme">
								<i class="fa fa-check"></i> Save Changes
							</button>
						</form>
					</div>
				</div>
		</div>
	</div>
	</section>
	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
<link
	href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/dt/css/buttons.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	

<script
	src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
	type="text/javascript"></script>

<script
	src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
	type="text/javascript"></script>	

<script type="text/javascript">
function changePwd() {
	
			var oldPwd = $("#oldInputPasswd").val().trim();
			var newPwd = $("#newInputPasswd").val().trim();
			var retypeNewPwd = $("#retypeInputPasswd").val().trim();
			var js1 = new Object();
			js1.oldPwd = oldPwd;
			js1.newPwd = newPwd;
			js1.retypeNewPwd = retypeNewPwd;
			if (newPwd == retypeNewPwd) {
				var json = {
					'json' : JSON.stringify(js1)
				};
				alert('updatePassword');
				ajaxCallRequestResponse('json',json,'post','changePassword',pInfo);
				//ajaxCallRequestResponse('json',json,'get','../updatePassword',pInfo);
			} else
				alert('please enter same password!'+oldPwd+''+ newPwd +''+ retypeNewPwd);

		}
		function pInfo(res) {
			var obj = JSON.stringify(res);
			var jsonobject = JSON.parse(obj);
			alert(jsonobject.data);
			if (jsonobject.data != '0')
				$('#pInfo').html(jsonobject.data).slideDown('slow');
			else {
				//$('.row message').hide();
				$('#pInfo').html(
						'<div class="alert alert-danger">'
								+ '<strong>No results found!' + '</div>')
						.slideDown('slow');
			}
		}

		</script>
</body>
</html>