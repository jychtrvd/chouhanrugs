
<%@include file="buyerTopHead.jsp"%>
</head>
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header">
		</div>
		<div class="row clearfix">
				<div class="row">
					<div class="col-xs-12">&#8201;
							<table id="example" class="display" width="100%">
			
							</table>
					</div>
				</div>
		</div>
	</div>
	</section>

	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
	
<link
	href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/dt/css/buttons.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	

<script
	src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
	type="text/javascript"></script>

<script
	src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
	type="text/javascript"></script>	

	<script>
	var num=0;
	 $(document).ready(function(){	
		 callDattableMethod('1','1');
	 });
		var columnData=[ 
{"title" : "Sr No.","render" : function(data, type, row, meta) {num=num+1;return num;}},

{"title" : "Order No","render" :  function(data, type, row, meta) {  
	
	return row[1];
	} },  
	{"title" : "Name","render" :  function(data, type, row, meta) {  
		
		return row[2];
		} }, 
		
		{"title" : "Total","render" :  function(data, type, row, meta) {  
			
			return row[3];
			} }, 		
	{"title" : "Cost","render" :  function(data, type, row, meta) {  
			
			return row[4];
			} },
	{"title" : "Description","render" :  function(data, type, row, meta) {  
		return "<a href=\"orderInfo?ordNo=" + row[1] + "&scope="+$('input[name=pval]').val()+'"'+" class=\"btn btn-success\" >details</a>";
				
				} } 
		  ];
	
		 var buttonData=[];
		var callDattableMethod=function(p1,p2){
			var d=new Object();
			var id='#example';
		    d.p1 = p1;
		    d.p2 = p2;
		    d.cas = 'dfdfdf';
			callDataTableWith(d,id,getUrl('../../getOrderList'),'get',columnData,buttonData);
			
		}
	 
	</script>	

</body>
</html>