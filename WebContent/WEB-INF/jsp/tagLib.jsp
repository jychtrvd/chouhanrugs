<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<fmt:bundle basename="trasaction">
       <fmt:message var="charge_free" key="charge.free"/>
       <fmt:message var="charge_corrier" key="charge.corrier"/>
       <fmt:message var="currency_dollar" key="currency.dollar"/>
       
        <fmt:message var="company_introduction" key="company.introduction"/>
       <fmt:message var="company_name" key="company.name"/>
       <fmt:message var="company_director_name" key="company.director.name"/>
       <fmt:message var="company_director_visible" key="company.director.visible"/>
       <fmt:message var="company_address" key="company.address"/>
       <fmt:message var="company_contact" key="company.contact"/>
       <fmt:message var="company_email" key="company.email"/>
        <fmt:message var="company_url" key="company.url"/>
        <fmt:message var="company_facebook" key="company.facebook"/>
        <fmt:message var="company_twitter" key="company.twitter"/>
        <fmt:message var="company_instagram" key="company.instagram"/>
        <fmt:message var="company_map" key="company.map"/>
        <fmt:message var="company_stableYear" key="company.stableYear"/>
        <fmt:message var="company_visible" key="company.director.visible"/>
      
 </fmt:bundle>