 <!-- head -->
			<%@include file="adminHead.jsp"%>
	<!-- End head -->
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
	
		<div class="block-header"></div>
		<div class="row clearfix">
			<div class="row">
				<div class="col-xs-12">
					&#8201;

					<div class="panel panel-default">
						<span class="label label-default">Order Information</span>
						<div class="table table-bordered table-responsive table-hover">
							<div id="tab1" style="display: block;">
								<table class="table table-bordered table-responsive table-hover">
									<tbody>
										<tr>
											<th>Product Image</th>
											<th>Product name</th>
											<th>Product Key</th>
											<th>Price</th>
											<th>Selling Price</th>
											<th>Quantity</th>
											<th>Shipping Charge</th>
											<th>Total</th>
										</tr>
										<c:forEach items="${itemDetail}" var="num">
											<tr>
												<td class="image-column"><a target="_blank"
													href="${pageContext.request.contextPath}/single?pkey=${num.pkey}">
														<img
														src="${pageContext.request.contextPath}/resources/images/Shopping_cart4.png"
														width="40px" height="40px" alt="view">
												</a></td>
												<td>${num.title}</td>
												<td>${num.pkey}</td>
												<td>${num.mrp}</td>
												<td>${num.smrp}</td>
												<td>${num.qty}</td>
												<c:set var="sValue"
															value="${sValue=sValue+1}" />
													
													<c:set var="totalValue"
														value="${totalValue=totalValue+num.smrp*num.qty}" />
												<c:if test="${totalValue gt free}">
												<td>&#x24;0</td>
												<td>${num.smrp*num.qty}</td>
												</c:if>
												<c:if test="${totalValue eq free}">
												<td>&#x24;0</td>
													<td><strong>&#x24;Free shipping</strong></td>
												</c:if>
												<c:if test="${totalValue le free}">
												<td>&#x24;${courier}</td>
												<td>${num.smrp*num.qty+courier*num.qty}</td>
												</c:if>
											</tr>
										</c:forEach>

										<tr>
											<td class="align-right" colspan="4"><span
												class="price big">Total</span></td>
											<c:if test="${totalValue gt free}">
												<td><strong>&#x24;${totalValue}</strong></td>
												<td><strong>&#x24;Free shipping</strong></td>
												<td>&#x24;${totalValue}</td>
											</c:if>
											<c:if test="${totalValue eq free}">
												<td><strong>&#x24;${totalValue}</strong></td>
												<td><strong>&#x24;Free shipping</strong></td>
												<td>&#x24;${totalValue}</td>
											</c:if>
											<c:if test="${totalValue le free}">
												<td><strong>&#x24;${totalValue}</strong></td>
												<td><strong>&#x24;${sValue*courier}</strong></td>
												<td>&#x24;${totalValue+sValue*courier}</td>
											</c:if>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>

			</div>
			
			<div class="row">
				<div class="col-xs-12">
					&#8201;
					<div class="panel panel-default">
						<span class="label label-default">Payment Information</span>
						<div class="table table-bordered table-responsive table-hover">
						<c:set var="payConfirm" value="0" scope="page" />
						<c:forEach items="${orderDetail}" var="innerList">
						
						<c:set var="payConfirm" value="${innerList[3]}" scope="page" />
							<table class="table table-bordered table-hover">
								<tbody>
									<tr>
										<th>Order number</th>
										<td>${innerList[0]} </td>
									</tr>

									<tr>
										<th>Order date</th>
										<td>
										${innerList[2]}</td>
									</tr>

									<tr>
										<th>Payment status</th>
							<td>
										 <c:choose>
											<c:when test="${innerList[4] eq 1}">						
<jsp:useBean id="trasactionHtml" class="com.bean.JsonBean">
<jsp:setProperty name="trasactionHtml" property="jsonHtml" value="${innerList[5]}" />  
</jsp:useBean>			   
${trasactionHtml.getTrasactionPayment()}										   
											    </c:when>
												<c:otherwise>
												    pending
												    </c:otherwise>
											</c:choose>
											</td>
									</tr>
									<tr>
										<th>Shipment</th>
										<td  id="shipmentResult">
										 <c:choose>
										 		<c:when test="${innerList[4] eq -1}">
											        
											         <button type="button" onclick="updateOrder(-1,'${innerList[0]}','update_order');" class="btn btn-warning waves-effect">Cancel Order</button>
											       <button type="button" onclick="updateOrder(1,'${innerList[0]}','update_order');" class="btn btn-success waves-effect">Confirm Dispatch</button>
											    </c:when>
												<c:when test="${innerList[4] eq 1}">
											       <c:if test="${innerList[3] eq 0  }">
						                                <button type="button" onclick="updateOrder(1,'${innerList[0]}','update_order');" class="btn btn-success waves-effect">Confirm Dispatch</button>
						                                <button type="button" onclick="updateOrder(-1,'${innerList[0]}','update_order');" class="btn btn-warning waves-effect">Cancel Order</button>
											       </c:if>
											       <c:if test="${innerList[3] eq -1}">
											        	Order Cancel by admin
											       </c:if>
											        <c:if test="${innerList[3] eq 1}">
											       		Order dispatch
											       </c:if>
											    </c:when>
											   <c:when test="${innerList[4] eq 0}">
											        <c:if test="${innerList[3] eq 0}">
						                                <button type="button" onclick="updateOrder(-1,'${innerList[0]}','update_order');" class="btn btn-warning waves-effect">Cancel Order</button>
						                                <button type="button" onclick="updateOrder(1,'${innerList[0]}','update_order');" class="btn btn-success waves-effect">Confirm Dispatch</button>
											       </c:if>
											        <c:if test="${innerList[3] eq -1}">
											        	Order Cancel
											       </c:if>
											       
											    </c:when>
												<c:otherwise>
												    -----
												    </c:otherwise>
											</c:choose>
										
										</td>
									</tr>
									<tr>
										<th>Total</th>
										<td><span class="price">${innerList[1]}</span></td>
									</tr>
								</tbody>
							</table>
</c:forEach>




						</div>
					</div>

				</div>
				<!-- /.row -->
			</div>
			
			
			<c:if test="${payConfirm eq 1}">
			<div class="row">
				<div class="col-xs-12">
					&#8201;

					<div class="panel panel-default">
						<span class="label label-default">Order confirmation</span>
						<div id="pInfo2"></div>
						<div class="table table-bordered table-responsive table-hover">
							<div id="tab1" style="display: block;">
								<table class="table table-bordered table-responsive table-hover">
									<tbody>
										<tr>
											<th>Product Image</th>
											<th>Product name</th>
											<th>Quantity</th>
											<th>Action</th>
											<th>Status</th>
										</tr>
										<c:set var="confItem" value="0" scope="page" />
										<c:set var="cancelItem" value="0" scope="page" />
										<c:set var="totalItems" value="0" scope="page" />
										<c:forEach items="${itemDetail}" var="num">
										<c:set var="totalItems" value="${totalItems+1}" scope="page" />
										<c:set var="orderID" value="${num.orderId}" />
											<tr>
												<td class="image-column"><a target="_blank"
													href="${pageContext.request.contextPath}/single?pkey=${num.pkey}">
														<img
														src="${pageContext.request.contextPath}/resources/images/Shopping_cart4.png"
														width="40px" height="40px" alt="view">
												</a></td>
												<td>${num.title}</td>
												<td>${num.qty}</td>
													<td id="info">						
												<c:if test="${num.confirm eq 0}">
							
								
			<label class="radio-inline">
			<input type="radio"	name="optradio" value="1">confirm</label>
			<label class="radio-inline">
			<input type="radio"	name="optradio" value="-1">out of stock</label>

			<button type="submit" class="btn btn-primary" onclick="proDetails('${num.orderId}','item_confirm','${num.gid}');"></button>
							   
						
												</c:if> 
												<c:if test="${num.confirm eq 1}">
												<c:set var="confItem" value="${confItem+1}" scope="page" />
												 confirm  
												</c:if>
												<c:if test="${num.confirm eq -1}">
												<c:set var="cancelItem" value="${cancelItem+1}" scope="page" />
												 canceled
												</c:if>
												
												</td>
												
												
												
												
												<td>
												<c:if test="${num.confirm eq 0}">
												 pending  
												</c:if>
												<c:if test="${num.confirm eq 1}">
												 <p><span class="glyphicon glyphicon-ok"></span></p>
												</c:if>
												<c:if test="${num.confirm eq -1}">
												  <p><span class="glyphicon glyphicon-remove"></span></p>  
												</c:if>

												</td>
													
												
											
												
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							
						</div>
					</div>

				</div>

			</div>
			
			
			
			<div class="row">

				<div class="col-xs-12">
					&#8201;

					<div class="panel panel-default">
						<span class="label label-default">Dispatch Courior Information</span>
						
					<c:if test="${confItem+cancelItem eq totalItems and confItem ne 0}">
					
					<c:if test="${empty couriorDetail or couriorDetail eq null}">
					<div class="table table-bordered table-responsive table-hover">
						<div id="pInfo"></div>
						<div id="ship_info" class="panel panel-default">
										<div class="panel-heading">
										
											<div class="form-group">
												<label for="email">Courier Company Name:</label> <input
													type="text" class="form-control" name="cName" id="cName"
													placeholder="Courier Company Name">
											</div>
											<div class="form-group">
												<label for="pwd">Courier Company Track Number:</label> <input
													type="text" class="form-control " name="cTrack" id="cTrack"
													placeholder="Courier Company Track Number">
											</div>
<button type="button" onclick="submitShip('${param.ordNo}','submit_ship','0');" class="btn btn-success waves-effect">Submit</button>
											
										</div>
									</div>
									</div>
									</c:if>
						
						
						<c:if test="${not empty couriorDetail or couriorDetail eq null}">
							<c:forEach items="${couriorDetail}" var="innerList2">
							<table class="table table-bordered table-hover">
								<tbody>
									<tr>
										<th>company name</th>
										<td>${innerList2[0]} </td>
									</tr>
									<tr>
										<th>tracking id</th>
										<td>
										${innerList2[1]}</td>
									</tr>
								</tbody>
							</table>
                               </c:forEach>
						</c:if>
					
					
					</c:if>
					<c:if test="${confItem+cancelItem ne totalItems}">
					
					please varified order items
					
					
					</c:if>
					<c:if test="${confItem eq 0}">
					
					order canceled items not available!
					
					
					</c:if>
					
					
					
					
						
					</div>

				</div>
				<!-- /.row -->
			</div>
			</c:if>
			
			
			<div class="row">

				<div class="col-xs-12">
					&#8201;

					<div class="panel panel-default">

						<div class="table table-bordered table-responsive table-hover">

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="carousel-heading">
									<h4>Bill to</h4>
								</div>
								<table class="orderinfo-table table-responsive">
									<tbody>
										<tr>
											<th>Name</th>
											<td>${regInfo.name}</td>
										</tr>
										<tr>
											<th>Address 1</th>
											<td>${regInfo.street}</td>
										</tr>
										<tr>
											<th>Address 2</th>
											<td>${regInfo.address}</td>
										</tr>
										<tr>
											<th>ZIP / Postal code</th>
											<td>${regInfo.pincode}</td>
										</tr>

										<tr>
											<th>City</th>
											<td>${regInfo.city}</td>
										</tr>
										<tr>
											<th>State</th>
											<td>${regInfo.state}</td>
										</tr>
										<tr>
										<tr>
											<th>Country</th>
											<td>India</td>
										</tr>
										<tr>
											<th>Phone</th>
											<td>+91-${regInfo.mobile}</td>
										</tr>
									</tbody>
								</table>

							</div>

							<div class="col-lg-6 col-md-6 col-sm-6">

								<div class="carousel-heading">
									<h4>Ship to</h4>
								</div>

								<table class="orderinfo-table">
									<tbody>
										<tr>
											<th>Name</th>
											<td>${shippingAdd.sName}</td>
										</tr>
										<tr>
											<th>Address 1</th>
											<td>${shippingAdd.sAddress1}</td>
										</tr>
										<tr>
											<th>Address 2</th>
											<td>${shippingAdd.sAddress2}</td>
										</tr>
										<tr>
											<th>ZIP / Postal code</th>
											<td>${shippingAdd.sPincode}</td>
										</tr>

										<tr>
											<th>City</th>
											<td>${shippingAdd.sCity}</td>
										</tr>
										<tr>
											<th>State</th>
											<td>${shippingAdd.sState}</td>
										</tr>
										<tr>
										<tr>
											<th>Country</th>
											<td>India</td>
										</tr>
										<tr>
											<th>Phone</th>
											<td>+91-${shippingAdd.sMobile}</td>
										</tr>
									</tbody>
								</table>
							</div>


						</div>
					</div>

				</div>
				<!-- /.row -->
			</div>
		</div>
	</div>
	</section>
	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
	<link
		href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
		rel="stylesheet" type="text/css" />
	<link
		href="${pageContext.request.contextPath}/dt/css/buttons.dataTables.min.css"
		rel="stylesheet" type="text/css" />


	<script
		src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
		type="text/javascript"></script>

	<script type="text/javascript">
	contextPath='<%=request.getContextPath()%>';
	function updateOrder(value,orderId,con){
			var js1=new Object();
			js1.value=value;
			js1.condition=con;
			js1.orderId=orderId;
			var json={'json':JSON.stringify(js1)} ;
			ajaxCallRequestResponse('json',json,'post','orderInfo',output);
	
	}
	function output(res){
		var str='';
		alert(res.data);
		for (var key in res) {
		    str+=res[key]+'\n'
		}
		 document.getElementById('shipmentResult').innerHTML = str;
		 location.reload();
	}
	
	function updateItem()
	{
		alert('hello');
	}
	
	function proDetails(orderId, con, gid) {
		if ($('input[name=optradio]').is(":checked")) {
			var opt = $('input[name=optradio]:checked').val();
			alert('Sample=='+$("#hGid").text(gid)+' opt=  '+opt);
			$("#hGid").text(gid);
				var js1 = new Object();
				js1.orderId = orderId;
				js1.opt = opt;
				js1.gid = gid;
				js1.condition = con;
				var json = {
					'json' : JSON.stringify(js1)
				};
				ajaxCallRequestResponse('json', json, 'post','orderInfo', output3);
			
		} else
			alert('please select option');
	}
	function output3(response) {
		var obj = JSON.stringify(response);
		var jsonobject = JSON.parse(obj);
		if (jsonobject.status){
			$('#pInfo2').html(jsonobject.data).slideDown('slow');
			location.reload();
			}
		else {
			$('#pInfo2').html(
		'<div class="alert alert-danger"><strong>No results found!' + '</div>').slideDown('slow');
		}
	}
	function submitShip(orderId, con, gid) {
		var js1 = new Object();
		js1.condition = con;
		js1.name = $('#cName').val().trim();
		js1.trackId = $('#cTrack').val().trim();
		js1.orderId = orderId;
		js1.gid = gid;
		if (js1.name != '' & js1.trackId != '') {		
		
				var json={'json':JSON.stringify(js1)} ;
				ajaxCallRequestResponse('json',json,'post','orderInfo',output2);
				$("#ship_info").hide();
				}
		 else
		$('#pInfo').html('<span class=\"label label-danger\">Insert Courior Company Name and valid tracking Id!</span>');
	}
	
	function output2(response) {
		var obj = JSON.stringify(response);
		var jsonobject = JSON.parse(obj);
		$('#info').empty();
		if (jsonobject.status){
			$('#pInfo').html(jsonobject.data).slideDown('slow');
			location.reload();
			}
		else {
			//$('.row message').hide();
			$('#pInfo').html(
					'<div class="alert alert-danger">'
							+ '<strong>No results found!' + '</div>')
					.slideDown('slow');
		}
	}
		
	</script>
</body>
</html>