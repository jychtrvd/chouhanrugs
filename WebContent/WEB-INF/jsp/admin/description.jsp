
<!-- head -->
<%@include file="adminHead.jsp"%>
<link href="../resources/dt/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link href="../resources/admin/css/description.css" rel="stylesheet"
	type="text/css" />
	<link rel="stylesheet" href="../resources/admin/css/admin_slide.css">
<style>

.img-wraps {
    position: relative;
    display: inline-block;
   
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 5px;
    right: 79px;
    z-index: 100;
    background-color: #FFF;
    padding: 4px 3px;
    
    color: #000;
    font-weight: bold;
    cursor: pointer;
   
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}

</style>	
	
<!-- End head -->
<body class="theme-red">
	${error}
	<c:set var="rand"
		value="<%=java.lang.Math.round(java.lang.Math.random() * 100000)%>"
		scope="session"></c:set>
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->
	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	<!-- Right Sidebar --> <%@include file="right_sidebar.jsp"%>
	<!-- #END# Right Sidebar --> </section>
	<section class="content">
		<c:forEach var="fp" items="${product_desc}">
			<c:set var="sUrl" value="${fn:split(fp.url,',')}" />
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="preview col-md-6">
					<div class="w3-content" style="max-width:1200px">
	<c:if test="${fn:length(sUrl) > 0}">	
		<c:forEach var="i" begin="0" end="${fn:length(sUrl)-1}">			
  <img class="mySlides" src="${pageContext.request.contextPath}/img/fixed/${sUrl[i]}" style="width: 280px;height: 415px;">
</c:forEach>
<c:set var="num" value="3" />
  <div class="w3-row-padding w3-section">
  <c:forEach var="i" begin="0" end="${fn:length(sUrl)-1}">
    
      <img class="demo w3-opacity w3-hover-opacity-off" src="${pageContext.request.contextPath}/img/smaller/${sUrl[i]}" style="width: 60px;height: 80px;" onclick="currentDiv((${i+1}))">
    <!--   <a href="#" onclick="imgCall('${sUrl[i]}')">delete</a>  -->
    
    </c:forEach>
  </div>
  </c:if>	
</div>
					</div>
					<div class="details col-md-6">
							<h3 class="product-title">${fp.title} </h3>
							<div class="rating">
								<div class="stars">
									<span class="fa fa-star checked"></span> <span
										class="fa fa-star checked"></span> <span
										class="fa fa-star checked"></span> <span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
								<span class="review-no">0 reviews</span>
							</div>
							<p class="product-description">${fp.description}</p>
							<h4 class="price">
								current price: <span>${fp.mrp}</span> 

							</h4>
							<h4 class="price">
								Selling price: <span>${fp.smrp}</span> 
							</h4>
							<h5 class="sizes">
								sizes: <span class="size" data-toggle="tooltip"
									title="${fp.size}">${fp.size}</span>
							</h5>
							<h5 class="count">
								Product: <span class="count" data-toggle="tooltip"
									title="${fp.count}">${fp.count}</span>
							</h5>
							<h5 class="colors">
								colors: <span 
									data-toggle="tooltip" title="avaiable">${fp.color}</span>
							</h5>
							
							<div class="action">
							
							</div>
							<div class="action">
							<button type="button" class="add-to-cart btn" data-toggle="modal" onclick="goBack()" style="margin-bottom: 20px;">back</button>
							<button type="button" class="add-to-cart btn" data-toggle="modal" data-target="#myModal" style="margin-bottom: 20px;">Update Product</button>
								<c:if test="${fp.visible==1}">
									<input type="submit" name="submitButton"
										onclick="enableDisable('disable')" class="add-to-cart btn" value="Disable Product" style="margin-bottom: 20px;">
								</c:if>
								<c:if test="${fp.visible==0}">
									<input type="submit" name="submitButton"
										onclick="enableDisable('enable')" class="add-to-cart btn" value="Enable Product" style="margin-bottom: 20px;">
								</c:if>
							</div>
							
					</div>
				</div>
			</div>
  <!-- Trigger the modal with a button -->
  

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Product Description</h4>
        </div>
        <div class="modal-body">
						<form id="product" method="post">


							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="size">Select Size:-(in cm)</label> 
										<select class="form-control" name="size" id="size">
											<option selected value="${fp.size}">${fp.size}</option>
											<option value="30*30">30*30</option>
											<option value="35*35">35*35</option>
											<option value="40*40">40*40</option>
											<option value="45*45">45*45</option>
											<option value="50*50">50*50</option>
											<option value="55*55">55*55</option>
											<option value="60*60">60*60</option>
											<option value="65*65">65*65</option>
											<option value="70*70">70*70</option>
											<option value="75*75">75*75</option>
											<option value="60*90">60*90</option>
											<option value="90*150">90*150</option>
											<option value="120*120">120*120</option>
											<option value="120*180">120*180</option>
											<option value="150*150">150*150</option>
											<option value="150*240">150*240</option>
											<option value="180*180">180*180</option>
											<option value="180*270">180*270</option>
											<option value="240*240">240*240</option>
											<option value="240*300">240*300</option>
											<option value="270*360">270*360</option>
											<option value="270*270">270*270</option>
											<option value="300*300">300*300</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="color">Select Color:-</label> 
										<select class="form-control" name="color" id="color">
											<option selected value="${fp.color}">${fp.color}</option>
											<option value="SAME AS IMAGE">SAME AS IMAGE</option>
											<option value="WHITE">WHITE</option>
											<option value="SILVER">SILVER</option>
											<option value="GRAY">GRAY</option>
											<option value="BLACK">BLACK</option>
											<option value="RED">RED</option>
											<option value="MAROON">MAROON</option>
											<option value="YELLOW">YELLOW</option>
											<option value="OLIVE">OLIVE</option>
											<option value="LIME">LIME</option>
											<option value="GREEN">GREEN</option>
											<option value="AQUA">AQUA</option>
											<option value="TEAL">TEAL</option>
											<option value="BLUE">BLUE</option>
											<option value="NAVY">NAVY</option>
											<option value="FUCHSIA">FUCHSIA</option>
											<option value="PURPLE">PURPLE</option>
											
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="mrp">Market Price:-</label> <input id="tmrp"
											class="form-control" name="mrp" type="text"
											placeholder="New Market price" value="${fp.mrp}"
											data-onload="isValidInput('txtcategories','')">
											
										
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="smrp">Selling Price:-</label> <input id="tsmrp"
											class="form-control" name="smrp" type="text" 
											placeholder="New Selling price" value="${fp.smrp}"
											data-onload="isValidInput('txtcategories','')">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="qty">Quantity:-</label> <input id="qty"
											class="form-control" name="qty" type="text"
											placeholder="Enter Quantiy" value="${fp.count}"
											data-onload="isValidInput('txtcategories','')">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="sample">Sample:-</label> <input
											class="form-control" name="sample" type="text"
											placeholder="Blank "
											data-onload="isValidInput('txtcategories','')">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="description">Description:-</label>
										<textarea class="form-control" rows="5"  id="description"
											placeholder="Enter Product Description" value="${fp.description}"
											name="description">${fp.description}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label for="sample">Upload Image:-</label> <input
										class="lowercase input-file uniform_on" id="datafile"
										type="file" name="datafile" />
									<div id="upload" style="display: none;">Uploading..</div>
									<div id="imgivew"  class="w3-row-padding w3-section">
									
									
									</div>
									<small id="fileHelp" class="form-text text-muted">Attach
										photograph related to products.</small>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<input type="hidden" name="pkey" value="${pro.pkey}"> <label
										for="submit">Submit</label> <input class="btn btn-default"
										type="submit" value="submit">
								</div>
							</div>
						</form>
					
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
		</c:forEach>
		<input type="hidden" name="pvid" value="${fp.visible}" /> 
		<input type="hidden" name="update" value="pv" />
	    <input type="hidden" name="pkey" value="pv" />

	</section>
	<!-- Js Files -->
	<%@include file="js_files.jsp"%>
	<!-- #END# Js Files -->

	<!-- ajax fileupload -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.ajaxfileupload.js"></script>
	<script
		src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
	contextPath='<%=request.getContextPath()%>';
		function UpdateIdinHF(opti) {
			
			var bol = false;
			//var id = $(obj).attr('id');
			//var name = $(obj).attr('name');
			//var value = $(obj).attr('value');
			//var IsChecked = $(obj).is(':checked');
			var js1 = new Object();
			//js1.name = name;
			//js1.value = value;
			js1.pkey = getUrlParameter('pkey');
			
			if ('field_update' == opti) {
				js1.smrp = $('#tsmrp').val();
				js1.mrp = $('#tmrp').val();
				js1.count = $('#qty').val();
				js1.description = $('#description').val();
				js1.size = $('#size').val();
				js1.color = $('#color').val();
			alert(js1.size+'   '+js1.color);
				
				if (js1.smrp > 0 & js1.mrp > 0) {
					console.log(bol+'  '+(js1.smrp <= js1.mrp));

						console.log(bol+'  '+(js1.smrp <= js1.mrp));
						bol = true;
						console.log(bol+'  '+(js1.smrp <= js1.mrp));
				}
			}
			if (bol) {
				js1.opt = opti;
				var json = {
					'json' : JSON.stringify(js1)
				};
				//ajaxRequest(json, 'get', '/admin/${sessionScope.admin}/menus',divTest);
				ajaxCallRequestResponse('json', json, 'post',
						'description', divTest);
			}

			// callDattableMethod();
		}
		function divTest(data) {
			
			var obj = JSON.stringify(data);
			var jsonobject = JSON.parse(obj);
			console.log('jsonobject : - '+jsonobject);
			console.log('1111111111111111');

			alert(jsonobject);
		//	$('.details').html(jsonobject.data);
			 location.reload();
		}
		function enableDisable(opti) {
			var bol = false;
			var js1 = new Object();
			js1.pkey = getUrlParameter('pkey');
			if ('disable' == opti | 'enable' == opti) {
				alert(' js1.pkey='+js1.pkey+'  button value='+opti);
				bol = true;
			}
			
			if (bol) {
				js1.opt = opti;
				var json = {
					'json' : JSON.stringify(js1)
				};
				//ajaxRequest(json, 'get', '/admin/${sessionScope.admin}/menus',divTest);
				ajaxCallRequestResponse('json', json, 'post',
						getUrl('/description'), divTest2);
			}
			

			// callDattableMethod();
		}
		
		function divTest2(data) {
			alert(data);
		//	$('.details').html(jsonobject.data);
			 location.reload();

			
		}
		function imgCall(msg){
			
		alert(msg);
		}
		
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  console.log(dots+' : '+x+'  :  '+n.src);
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
     console.log(i+" -- i--"+x[i].src);
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-opacity-off";
}


$(function() {
	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /[a-zA-Z]+$/i.test(value);
	}, "Letters only please with no space");
	jQuery.validator.addMethod("noSpace", function(value, element) {
		return value.indexOf(" ") < 0 && value != "";
	}, "No space please and don't leave it empty");
	jQuery.validator.addMethod("size", function(value, element) {
		return this.optional(element) || value != -1;
	}, "No space please and don't leave it empty");
	jQuery.validator.addMethod("color", function(value, element) {
		return this.optional(element) || value != -1 || value != 0;
	}, "No space please and don't leave it empty");
	jQuery.validator.addMethod("modelselect", function(value, element) {
		return this.optional(element) || value != -1;
	}, "No space please and don't leave it empty");
	var ruleCall ={ title : "required",
			description : { required : true, noSpace : false }, 
			qty : { required : true,number: true } ,
			mrp : { required : true,number: true },
			smrp : { required : true,number: true },
			size:{required : true,size : true,number: false },
			color:{required : true,color : true},};
	var msg ={
			description : "Enter description",  
			qty : "Enter length in digit", 
			smrp : "Enter smrp in digit", 
			mrp : "Enter mrp in digit", 
			weight : "Enter weight in digit", 
			size : "Enter size", 
			color : "enter color"};  
	validatorAjax("#product", ruleCall, msg, 0);  });
	function submitForm(form,id){
			//  form.submit();
			alert('Update Action');
			
			UpdateIdinHF('field_update');
	}
	$(document).ready(
			function(){
				$('input[type="file"]').ajaxfileupload({
			   	      'action': '<%=request.getContextPath()%>/UploadFile?cate=multiple','type' : 'post',
					'onComplete' : function(response) {
						$('#upload').hide();
						$('#imgivew').empty();
						$.each($.parseJSON(response.url),
							function(index,value) {
							jQuery("#imgu").attr('src','../img/main/'+ value);
								$("#datafile").val('');
							//		alert(value);
								$('#imgivew').val(value);
								$('#imgivew').prepend('<div class="w3-col s4"><img ng-model="docUrl" ng-init="docUrl='+value+'" width="100px" height="50px" alt="document upload" id="docUrl" name="docUrl" class="img-thumbnail img-responsive"  src="../img/main/'+value+'" style=" height: 100px; width: 55px;"/>'
																						
								+"<a href=\"#\" onclick=\"img_remove(\'"+value+"','"+getUrlParameter("pkey")+"')\">delete</a></div>");
								});
							},
						'onStart' : function() {
						$('#upload').show();
								}
					});
						});
		// number: true


		
		function imgremove(msg,pky){
			alert(msg+' : '+pky);
		}
		
		
	var img_remove=function(msg,pky){
		 $.ajax({  
             url:'../img_response',  
             type:'post',  
             dataType: 'json',
             data : {
 				_msg : msg,
 				_pky:pky
 				
 			},
             success: function(data) {  
					$('#imgivew').html(data.url);
					console.log(data.url);
             }  
         });   
	}
	
	function goBack() {
	    window.history.back();
	}
	</script>

</body>
</html>