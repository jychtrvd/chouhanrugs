<!-- head -->
<%@include file="adminHead.jsp"%>
<!-- End head -->
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header">
		</div>

		<!-- Widgets -->
		
		<!-- #END# Widgets -->
		<!-- CPU Usage -->
		<!--
		<!-- #END# CPU Usage -->
		<div class="row clearfix">
			<!-- Answered Orders -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="card">
					<div class="body bg-teal">
						<div class="font-bold m-b--35">Order Summery</div>
						<ul class="dashboard-stat-list">
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">sentiment_satisfied_alt</i>
					</div>
					<div class="content">
						<div class="text">Total Orders</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.totalOrder}</div>
					</div>
				</div>
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">list_alt</i>
					</div>
					<div class="content">
						<div class="text">Pending Orders</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.pendingOrder}</div>
					</div>
				</div>
				<div class="info-box bg-light-green hover-expand-effect">
					<div class="icon">
						<i class="material-icons">playlist_add_check</i>
					</div>
					<div class="content">
						<div class="text">Order Success</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.successfullOrder}</div>
					</div>
				</div>
				
				<div class="info-box bg-red hover-expand-effect">
					<div class="icon">
						<i class="material-icons">cancel</i>
					</div>
					<div class="content">
						<div class="text">Order Cancel</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.cancelOrder}</div>
					</div>
				</div>
				
							
						</ul>
					</div>
				</div>
			</div>  
			<!-- #END# Answered Orders -->
			
			<!-- Answered Payments -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="card">
					<div class="body bg-teal">
						<div class="font-bold m-b--35">Payment Summery</div>
						<ul class="dashboard-stat-list">
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">payment</i>
					</div>
					<div class="content">
						<div class="text">Total Payments</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.totalPayments}</div>
					</div>
				</div>
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">list_alt</i>
					</div>
					<div class="content">
						<div class="text">Pending Payments</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.pendingPayments}</div>
					</div>
				</div>
				<div class="info-box bg-light-green hover-expand-effect">
					<div class="icon">
						<i class="material-icons">playlist_add_check</i>
					</div>
					<div class="content">
						<div class="text">Payments Success</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.successFullPayments}</div>
					</div>
				</div>
				
				<div class="info-box bg-red hover-expand-effect">
					<div class="icon">
						<i class="material-icons">cancel</i>
					</div>
					<div class="content">
						<div class="text">Payments Cancel</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.cancelPayments}</div>
					</div>
				</div>
				
				
				
							
						</ul>
					</div>
				</div>
			</div>  
			<!-- #END# Answered Payments -->
			
			<!-- Answered Products -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="card">
					<div class="body bg-teal">
						<div class="font-bold m-b--35">Products Summery</div>
						<ul class="dashboard-stat-list">
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">store</i>
					</div>
					<div class="content">
						<div class="text">Total Products</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.totalProducts}</div>
					</div>
				</div>
				<div class="info-box bg-cyan hover-expand-effect">
					<div class="icon">
						<i class="material-icons">list_alt</i>
					</div>
					<div class="content">
						<div class="text">Pending Products</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.pendingProductApproval}</div>
					</div>
				</div>
				<div class="info-box bg-light-green hover-expand-effect">
					<div class="icon">
						<i class="material-icons">playlist_add_check</i>
					</div>
					<div class="content">
						<div class="text">Available Products</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.availableProducts}</div>
					</div>
				</div>
				
				<div class="info-box bg-red hover-expand-effect">
					<div class="icon">
						<i class="material-icons">cancel</i>
					</div>
					<div class="content">
						<div class="text">Out Of Stock</div>
						<div class="number count-to" data-from="0" data-to="125"
							data-speed="15" data-fresh-interval="20">${summery.disableProducts}</div>
					</div>
				</div>
						</ul>
					</div>
				</div>
			</div> 
			<!-- #END# Answered Products -->
		</div>
		
		

		<div class="row clearfix">
			<!-- Task Info -->
			<!-- #END# Task Info -->
			<!-- Browser Usage -->

			<!-- #END# Browser Usage -->
		</div>
	</div>
	</section>

	<!-- Js Files -->
	<%@include file="js_files.jsp"%>
	<!-- #END# Js Files -->


</body>
</html>