 <!-- head -->
			<%@include file="adminHead.jsp"%>
	<!-- End head -->
<link	href="../resources/dt/css/jquery.dataTables.min.css"	rel="stylesheet" type="text/css" />

<body class="theme-red">
	<c:set var="rand"
		value="<%=java.lang.Math.round(java.lang.Math.random() * 1000000)%>"
		scope="session"></c:set>
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
	

		<!-- Widgets -->
		<div class="row clearfix">
			
		</div>
		<!-- #END# Widgets -->
		<!-- CPU Usage -->

		<!-- #END# CPU Usage -->
		<div class="row clearfix">
			<!-- Visitors -->
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<table id="example" class="display" width="100%">
			
		</table>
			</div>
			<!-- #END# Visitors -->
			<!-- Latest Social Trends -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					
			</div>
			<!-- #END# Latest Social Trends -->
		</div>

		<div class="row clearfix">
			<!-- Task Info -->
			<a href="${pageContext.request.contextPath}/logout">Logout</a>
			<!-- #END# Task Info -->
			<!-- Browser Usage -->

			<!-- #END# Browser Usage -->
		</div>
	</div>
	</section>
 <input type="hidden" name="pval" value="${rand}"> 
	<!-- Js Files -->
	<%@include file="js_files.jsp"%>
	<!-- #END# Js Files -->
<script
	src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"	type="text/javascript"></script>
	
	<script>
	contextPath='<%=request.getContextPath()%>';
	 $(document).ready(function(){	
		 callDattableMethod('1','1');
	 });
		var columnData=[ 
{"title" : "Product Name","render" : function(data, type, row, meta) {return row[1];}},

{"title" : "view","render" :  function(data, type, row, meta) {  
	
	return "<a href=\"addProduct?pkey="+row[0]+"&scope="+$('input[name=pval]').val()+'"'+" class=\"btn btn-success\" >details</a>";
	} },  
		  ];
	
		 var buttonData=[];
		var callDattableMethod=function(p1,p2,cas){
			var d=new Object();
			var id='#example';
		    d.p1 = p1;
		    d.p2 = p2;
		    d.cas = 'dfdfdf';
			callDataTableWith(d,id,getUrl('/getProductList'),'get',columnData,buttonData);
			
		}
	 
	</script>	
</body>
</html>