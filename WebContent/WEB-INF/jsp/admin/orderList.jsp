 <!-- head -->
			<%@include file="adminHead.jsp"%>
	<!-- End head -->

<c:set var="rndMail" scope="page"><%= java.lang.Math.round(java.lang.Math.random() * 99999) %></c:set>
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper" style="display: none;">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>

	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->
	<!-- Top Bar -->
	<!-- brand -->
	<%@include file="nav_bar.jsp"%>
	<!-- End brand -->


	<!-- #Top Bar -->
	<section> <!-- Left Sidebar --> <%@include
		file="left_sidebar.jsp"%> <!-- #END# Left Sidebar -->
	</section>

	<section class="content">
	<div class="container-fluid">
		<div class="block-header"></div>
		<div class="row clearfix">
			
			<div class="row">
				<div class="col-xs-12">
					&#8201;
					<table id="example" class="display" width="100%">
					</table>
				</div>
			</div>
		</div>
	</div>
	</section>
	
	<!-- Js Files -->
	<%@include file="../admin/js_files.jsp"%>
	<!-- #END# Js Files -->
	<link
		href="${pageContext.request.contextPath}/dt/css/jquery.dataTables.min.css"
		rel="stylesheet" type="text/css" />
	<script
		src="${pageContext.request.contextPath}/dt/js/jquery.dataTables.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/dt/js/dataTables.buttons.min.js"
		type="text/javascript"></script>

	<script>
	contextPath='<%=request.getContextPath()%>';
		var num = 0;
		$(document).ready(function() {
			var num = 0;
			console.log('eady');
			callDattableMethod('1', '1','');
		});
		var columnData = [

				{
					"title" : "Sr No.",
					"render" : function(data, type, row, meta) {
						num = num + 1;
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},

				{
					"title" : "Order No",
					"render" : function(data, type, row, meta) {

						return row[1];
					}
				},
				{
					"title" : "Name",
					"render" : function(data, type, row, meta) {

						return row[2];
					}
				},

				{
					"title" : "Total",
					"render" : function(data, type, row, meta) {

						return row[3];
					}
				},
				{
					"title" : "Cost",
					"render" : function(data, type, row, meta) {

						return row[4];
					}
				},
				{
					"title" : "Description",
					"render" : function(data, type, row, meta) {
						return "<a href=\"orderInfo?ordNo=" + row[1]
								+ "&scope=" + $('input[name=pval]').val() + '"'
								+ " class=\"btn btn-success\" >details</a>";
					}
				},
				{
					"title" : "mail",
					"render" : function(data, type, row, meta) {
						return "<a href=\"#\" onclick=\"mailCopy('"+row[1]+"')\">mail send</a>";

					}
				} ];
		var buttonData = [ {
			text : 'payment (pending)',
			action : function() {
				callDattableMethod('0', '0','payment_pending');
			}
		}, {
			text : 'payment (confirm)',
			action : function() {
				callDattableMethod('0', '1','payment_confirm');
			}
		}, {
			text : 'payment (reject)',
			action : function() {
				callDattableMethod('0', '-1','payment_reject');
			}
		}, {
			text : 'order (pending)',
			action : function() {
				callDattableMethod('0', '0','order_pending');
			}
		}, {
			text : 'order (dispatched)',
			action : function() {
				callDattableMethod('0', '0','order_dispatch');
			}
		}, {
			text : 'order (reject)',
			action : function() {
				callDattableMethod('0', '-1','order_reject');
			}
		}, {
			text : 'dispatch completed',
			action : function() {
				callDattableMethod('1', '0','');
			}
		} ];
		var callDattableMethod = function(p1, p2,cas) {
			var d = new Object();
			var id = '#example';
			d.p1 = p1;
			d.p2 = p2;
			d.cas = cas;

			callDataTableWith(d, id, getUrl('../../getOrderList'), 'get',
					columnData, buttonData);

		}
		
		function mailCopy(orderNo) {
			var js1 = new Object();
			js1.cate = 'mail';
			js1.orderNo = orderNo;
				var json = {
					'json' : JSON.stringify(js1)
				};
				ajaxCallRequestResponse('json',json,'post','orderList',mailres);
		}
		
		function mailres(res){
			console.log(res.data);
			
		}
		
		
		
	</script>

</body>
</html>