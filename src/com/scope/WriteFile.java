package com.scope;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;

public class WriteFile {

	public static boolean writeFileWithMsg(HttpServletRequest request,String msg){
		boolean bol =false;
		BufferedWriter bufferedWriter = null;
		try {
			//String strContent = "This example shows how to write string content to a file";
			String root = request.getServletContext().getRealPath("/");
			File path = new File(root + "/msg");
			if (!path.exists())
				path.mkdirs();
			File myFile = new File(path.getPath()+File.separator+"/MyTestFile.txt");
			System.out.println(myFile);
			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				myFile.createNewFile();
			}
			Writer writer = new FileWriter(myFile,true);
			bufferedWriter = new BufferedWriter(writer);
			bufferedWriter.append(msg);
			bol=true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null)
					bufferedWriter.close();
			} catch (Exception ex) {

			}
		}
		return bol;

	}
}
