package com.scope;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import com.model.OrderDetails;

public class CurrentTime {
	public static String getUTCTimeDate(String format){
		String str="";
		ZonedDateTime currentDate = ZonedDateTime.now( ZoneOffset.UTC );
		switch(format){
		case "hour":
			  str+=currentDate.getHour();
			  break;
        case "min":
			 str+=currentDate.getMinute(); 
			 break;	
        case "sec":
        	str+=currentDate.getSecond();
        	break;
        case "date":
        	str+=currentDate.getDayOfMonth();
        	break;
        case "month":
        	str+=currentDate.getMonthValue();
        	break;
        case "year":
        	str+=currentDate.getYear();
        	
        	break;
        
		}
		return str;
		
	}
	
	public static void main(String args[]){
		ZonedDateTime currentDate = ZonedDateTime.now( ZoneOffset.UTC );
		//System.out.println(currentDate.getMonthValue());
		System.out.println(CurrentTime.getUTCTimeDate("date"));
		System.out.println(CurrentTime.getUTCTimeDate("month"));
		System.out.println(CurrentTime.getUTCTimeDate("year"));
		System.out.println(CurrentTime.getUTCTimeDate("hour"));
		System.out.println(CurrentTime.getUTCTimeDate("min"));
	}
	public static Date getTimeZone(String zone){
		DateFormat inFormat3 = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
		/*	date=inFormat3.parse(OrderDetails.formatDateToString(date, "yyyy-MM-dd HH:mm:ss", "IST"));  */
			date=inFormat3.parse(OrderDetails.formatDateToString(date, "yyyy-MM-dd HH:mm:ss", zone));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
		
		
	}
	

}
//ZonedDateTime currentDate = ZonedDateTime.now( ZoneOffset.UTC );
//System.out.println(currentDate);