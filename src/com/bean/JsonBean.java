package com.bean;

import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONValue;

public class JsonBean {
	private String jsonHtml;

	public void setJsonHtml(String jsonHtml) {

		this.jsonHtml = jsonHtml;
	}

	public String getTrasactionPayment() throws JSONException {
		String jsonString = jsonHtml;
		JSONObject jObject = new JSONObject(jsonString);
		// JSONObject menu = jObject.getJSONObject("menu");
		Iterator iter = jObject.keys();
		String tableDesign = "<table class=\"col-sm-12 table-bordered table-striped table-condensed cf\">" + " <tbody>";
		while (iter.hasNext()) {
			tableDesign += "<tr>";
			String key = (String) iter.next();
			String value = jObject.getString(key);
			tableDesign += "<td><span class=\"label label-default\">" + key + "</span></td>";
			tableDesign += "<td><span class=\"label label-success\">" + value + "</span></td>";
			tableDesign += "</tr>";
		}
		tableDesign += "</tbody></table>";
		return tableDesign;
	}
}
