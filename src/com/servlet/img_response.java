package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class img_response
 */
@WebServlet("/img_response")
public class img_response extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public img_response() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String str="";

			String msg = request.getParameter("_msg").trim();
			String pky = request.getParameter("_pky").trim();
			ArrayList<String> arrlist = null;
			arrlist = (ArrayList<String>) request.getSession().getAttribute("img");
			arrlist.remove(msg);
			
			String folderName[] = { "smaller", "fixed","main" };
			
			for(int i=0;i<folderName.length;i++){
				String uploadPath = request.getServletContext().getRealPath("/") +File.separator+ "/img/"+folderName[i]+"/";
				File savedFile = new File(uploadPath + File.separator + msg);
				System.out.println("upload path:- "+savedFile.getPath());
				System.out.println("delete file  "+savedFile.delete());
			}
			
			
			
			
			
			request.getSession().setAttribute("img", arrlist);
//			if (arrlist != null) {
//				Iterator itr = arrlist.iterator();
//				while (itr.hasNext()) {
//					String img = (String) itr.next();
//					// System.out.println(img);
//					if (img.equals(msg)){
//						arrlist.remove(img);
//						String uploadPath = request.getServletContext().getRealPath("/") +File.separator+ "/temp/img/";
//						
//						File savedFile = new File(uploadPath + File.separator + msg);
//						System.out.println("upload path:- "+savedFile.getPath());
//						System.out.println("delete file  "+savedFile.delete());
//					}
//					break;
//				}
//			}
			
			Iterator itr = arrlist.iterator();
			while (itr.hasNext()) {
				String img = (String) itr.next();
				// System.out.println(img);
				str+="<div class=\"w3-col s4\"><img ng-model=\"docUrl\" ng-init=\"docUrl="+img+"\" width=\"100px\" height=\"50px\" alt=\"document upload\" id=\"docUrl\" name=\"docUrl\" class=\"img-thumbnail img-responsive\"  src=\"../img/smaller/"+img+"\" style=\" height: 100px; width: 55px;\" />"
					+"<a href=\"#\" onclick=\"img_remove(\'"+img+"','"+pky+"')\">delete</a></div>";
			}
			//request.getSession().setAttribute("img", arrlist);
			System.out.println(pky + ": " + msg);
			PrintWriter out = response.getWriter();
			JSONObject json = new JSONObject();
			json.put("url", str);
			out.print(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
