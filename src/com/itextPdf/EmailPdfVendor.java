package com.itextPdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.logic.CartLogic;
//import com.logic.MerchantLogic;
//import com.logic.RegistrationLogic;
import com.model.CartModel;
import com.model.LoginSession;
//import com.model.MerchantInfo;
import com.model.OrderDetails;
import com.model.Registration;
import com.model.ShippingModel;

public class EmailPdfVendor {
	private String recipient;
	private String subject;
	private String content;

	private String pdfTitle;
	private String pdfSubject;
	private String pdfKeywords;

	private BaseFont bfBold;
	private BaseFont bf;
	private int pageNumber = 0;

	private double total = 0;
	private int corrier = 0;

	/**
	 * Sends an email with a PDF attachment.
	 */

	public EmailPdfVendor(String recipient, String subject, String content, String pdfTitle, String pdfSubject,
			String pdfKeywords) {
		// this.recipient=recipient;
		this.recipient = "chaturvedi.ajay85@gmail.com";
		this.subject = subject;
		this.content = content;
		this.pdfTitle = pdfTitle;
		this.pdfSubject = pdfSubject;
		this.pdfKeywords = pdfKeywords;

	}

	public String email(String orderId, String gid, LoginSession ls) {
		System.out.println("order Id====" + orderId);
		String str = "";
		String smtpHost = "blagot.com"; // replace this with a valid host
		int smtpPort = 25; // replace this with a valid port
		String sender = "info@blagot.com"; 
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "false");
		properties.put("mail.smtp.host", "blagot.com");
		properties.put("mail.smtp.port", smtpPort);
		properties.setProperty("mail.smtp.**ssl.enable", "false");
		properties.setProperty("mail.smtp.**ssl.required", "false");
		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(sender, "ajay@1987");
			}
		});
		ByteArrayOutputStream outputStream = null;

		try {
			int i = 1;
			// construct the text body part
			MimeBodyPart textBodyPart = new MimeBodyPart();
			textBodyPart.setText(content);

			// now write the PDF content to the output stream
			outputStream = new ByteArrayOutputStream();
			writePdf(outputStream, orderId, gid, ls);
			byte[] bytes = outputStream.toByteArray();

			// construct the pdf body part
			DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
			MimeBodyPart pdfBodyPart = new MimeBodyPart();
			pdfBodyPart.setDataHandler(new DataHandler(dataSource));
			pdfBodyPart.setFileName(orderId + ".pdf");

			// construct the mime multi part
			MimeMultipart mimeMultipart = new MimeMultipart();
			mimeMultipart.addBodyPart(textBodyPart);
			mimeMultipart.addBodyPart(pdfBodyPart);

			// create the sender/recipient addresses
			InternetAddress iaSender = new InternetAddress(sender);
			InternetAddress iaRecipient = new InternetAddress(recipient);

			// construct the mime message
			MimeMessage message = new MimeMessage(session);
			message.setFrom((Address) new InternetAddress(sender));
			message.addRecipient(Message.RecipientType.TO, (Address) new InternetAddress(recipient));
			message.setSubject("sdsdsd");
			message.setContent(mimeMultipart);
			Transport.send((Message) message);

			// send off the email
			// Transport.send(mimeMessage);
			System.out.println(
					"sent from " + sender + ", to " + recipient + "; server = " + smtpHost + ", port = " + smtpPort);
			str += "sent from " + sender + ", to " + recipient + "; server = " + smtpHost + ", port = " + smtpPort;

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			// clean off
			if (null != outputStream) {
				try {
					outputStream.close();
					outputStream = null;
				} catch (Exception ex) {
				}
			}
		}
		return str;
	}

	public void writePdf1(OutputStream outputStream) throws Exception {
		Document document = new Document();
		PdfWriter.getInstance(document, outputStream);

		document.open();

		// document.addTitle("Invoice detail");
		// document.addSubject("Testing email PDF");
		// document.addKeywords("iText, email");

		document.addTitle(pdfTitle);
		document.addSubject(pdfSubject);
		document.addKeywords(pdfKeywords);

		document.addAuthor("blagot infotech");
		document.addCreator("blagot infotech");

		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk("hello!"));
		document.add(paragraph);
		document.close();
	}

	private void writePdf(OutputStream outputStream, String orderId, String gid, LoginSession ls) throws Exception {

		Document doc = new Document();
		PdfWriter docWriter = null;
		initializeFonts();
//		OrderDetails od = (OrderDetails) CartLogic.getOrderInfo(ls, orderId, 5);
		System.out.println("order ID: "+orderId);
		try {

//			JSONObject jsonObject = new JSONObject(od.getOther());
//			double charge = jsonObject.getInt("charge.free");
//			if (charge > od.getTotal()){
//				corrier = jsonObject.getInt("charge.corrier");;
//			}
			corrier=0;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CartModel cm1 =new  CartModel();// CartLogic.getCartModel(orderId, gid);
		ShippingModel sm = (ShippingModel) CartLogic.getOrderInfo(ls, orderId, 4,null);
		Registration reg =new Registration();// RegistrationLogic.getProductUserDetail(sm.getUid());
		//MerchantInfo mi = MerchantLogic.getCompanyDetail(cm1.getCompany_id());

		try {
			docWriter = PdfWriter.getInstance(doc, outputStream);
			;
			doc.addTitle(pdfTitle);
			doc.addSubject(pdfSubject);
			doc.addKeywords(pdfKeywords);
			doc.addAuthor("blagot infotech");
			doc.addCreator("blagot infotech");
			doc.setPageSize(PageSize.LETTER);
			doc.open();

			PdfContentByte cb = docWriter.getDirectContent();

			boolean beginPage = true;
			int y = 0;
			int i = 0;
			i++;
			if (beginPage) {
				beginPage = false;
				generateLayout(doc, cb);
				generateHeader(doc, cb, sm, reg);
				y = 615;
			}
			generateDetail(doc, cb, i, y, cm1);
			y = y - 15;
			if (y < 50) {
				printPageNumber(cb);
				doc.newPage();
				beginPage = true;
			}

			createHeadings(cb, 20, y - 20, "Vendor Address ");
			createHeadings_billShip(cb, 20, y - 60, sm.getsAddress1());
			createHeadings_billShip(cb, 20, y - 70, sm.getsAddress2());
			createHeadings_billShip(cb, 20, y - 80, sm.getsCity() + ", " + sm.getsState() + " - " + sm.getsPincode());
			createHeadings_billShip(cb, 20, y - 90, "India");

			// Vat or tin number
			createHeadings_billShip(cb, 200, y - 50, "Pan Number:- " + "pan number" + "");

			// GST number
			createHeadings_billShip(cb, 200, y - 60, "GST:-" + "GST number" + "");

			// createHeadings_billShip(cb, 150, y+5, "total:-");
			createHeadings(cb, 400, y + 2, "Sub total =");
			createHeadings_billShip(cb, 443, y + 2, " 250");
			createHeadings(cb, 400, y - 10, "Shipping =");
			createHeadings_billShip(cb, 443, y - 10, String.valueOf(corrier));
			/* total */
			createHeadings(cb, 400, y - 25, "Total = ");
			total=corrier+(Integer.parseInt(cm1.getSmrp())*cm1.getQty());
			createHeadings_billShip(cb, 430, y - 25, total + "/-");

			System.out.println("y value: " + y);
			cb.setLineWidth(1);
			cb.moveTo(10, 670);
			cb.lineTo(600, 670);
			cb.moveTo(20, y - 40);
			cb.lineTo(600, y - 40);
			cb.setLineWidth(0.05f);
			/* total line */
			cb.moveTo(400, y - 15);
			cb.lineTo(480, y - 15);
			cb.stroke();
			printPageNumber(cb);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
			if (docWriter != null) {
				docWriter.close();
			}
		}
	}

	private void generateLayout(Document doc, PdfContentByte cb) {

		try {

			cb.setLineWidth(1f);

			// Invoice Header box layout
			cb.rectangle(420, 700, 150, 60);
			cb.moveTo(420, 720);
			cb.lineTo(570, 720);
			cb.moveTo(420, 740);
			cb.lineTo(570, 740);
			cb.moveTo(480, 700);
			cb.lineTo(480, 760);
			cb.stroke();

			// Invoice Header box Text Headings
			createHeadings(cb, 422, 743, "Payment Type");
			createHeadings(cb, 422, 723, "Invoice No.");
			createHeadings(cb, 422, 703, "Invoice Date");

			// Invoice Detail box layout
			// cb.rectangle(20, 50, 550, 600);
			// cb.rectangle(20,300,550, 350);
			// cb.moveTo(20, 630);
			// cb.lineTo(570, 630);
			// cb.moveTo(50, 50);
			// cb.lineTo(50, 650);
			// cb.moveTo(150, 50);
			// cb.lineTo(150, 650);
			// cb.moveTo(430, 50);
			// cb.lineTo(430, 650);
			// cb.moveTo(500, 50);
			// cb.lineTo(500, 650);
			// cb.stroke();

			// Invoice Detail box Text Headings
			createHeadings(cb, 22, 660, "Sr No");
			createHeadings(cb, 52, 660, "Item Number");
			createHeadings(cb, 152, 660, "Item Description");
			createHeadings(cb, 432, 660, "Price");
			// createHeadings(cb, 502, 660, "Ext Price");

			// add the images
//			Image companyLogo = Image.getInstance("http://blagot.com/images/logo.png");
//			companyLogo.setAbsolutePosition(25, 700);
//			companyLogo.scalePercent(25);
//			doc.add(companyLogo);

		}

//		catch (DocumentException dex) {
//			dex.printStackTrace();
//		} 
	catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void generateHeader(Document doc, PdfContentByte cb, ShippingModel sm, Registration reg) {

		try {
			createHeadings(cb, 100, 750, "Shipping Address");
			createHeadings_billShip(cb, 100, 735, sm.getsName());
			createHeadings_billShip(cb, 100, 720, sm.getsState());
			createHeadings_billShip(cb, 100, 705, sm.getsAddress1());
			createHeadings_billShip(cb, 100, 690, sm.getsCity() + ", " + sm.getsState() + " - " + sm.getsPincode());
			createHeadings_billShip(cb, 100, 680, "Ph.-" + sm.getsMobile() + ",India");
			createHeadings_billShip(cb, 100, 670, "Country");

			createHeadings_billShip(cb, 482, 743, "Prepaid");
			createHeadings_billShip(cb, 482, 723, sm.getOrderId());
			createHeadings_billShip(cb, 482, 703, "date now" + "");

			// billing and shipping address
			createHeadings(cb, 250, 750, "Billing Address");
			createHeadings_billShip(cb, 250, 735, reg.getName());
			createHeadings_billShip(cb, 250, 705, reg.getStreet());
			createHeadings_billShip(cb, 250, 690, reg.getAddress());
			createHeadings_billShip(cb, 250, 680, reg.getCity() + ", " + reg.getState() + " - " + reg.getPincode());
			createHeadings_billShip(cb, 250, 670, "Ph.-" + reg.getMobile() + ",India");

		}

		catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void generateDetail(Document doc, PdfContentByte cb, int index, int y, CartModel cm) {
		DecimalFormat df = new DecimalFormat("0.00");

		try {
			createContent(cb, 30, y, String.valueOf(index), PdfContentByte.ALIGN_RIGHT);
			createContent(cb, 52, y, cm.getQty() + "", PdfContentByte.ALIGN_LEFT);
			createContent(cb, 152, y, cm.getTitle() + "-" + cm.getSize() + "-" + cm.getColor(),
					PdfContentByte.ALIGN_LEFT);

			double price = Double.valueOf(df.format(Math.random() * 10));
			double extPrice = price * (index + 1);
			createContent(cb, 320, y, cm.getSmrp(), PdfContentByte.ALIGN_RIGHT);
			createContent(cb, 420, y, df.format(extPrice), PdfContentByte.ALIGN_RIGHT);
			// createContent(cb, 520, y, df.format(extPrice),
			// PdfContentByte.ALIGN_RIGHT);
			System.out.println("size and color :- " + cm.getSize() + "-" + cm.getColor());

		}

		catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void createHeadings(PdfContentByte cb, float x, float y, String text) {

		cb.beginText();
		cb.setFontAndSize(bfBold, 8);
		cb.setTextMatrix(x, y);
		cb.showText(text.trim());
		cb.endText();

	}

	private void createHeadings_billShip(PdfContentByte cb, float x, float y, String text) {
		try {
			BaseFont font = BaseFont.createFont();

			cb.beginText();
			cb.setFontAndSize(font, 8);
			cb.setTextMatrix(x, y);
			cb.showText(text.trim());
			cb.endText();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void printPageNumber(PdfContentByte cb) {

		cb.beginText();
		cb.setFontAndSize(bfBold, 8);
		cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page No. " + (pageNumber + 1), 570, 25, 0);
		cb.endText();

		pageNumber++;

	}

	private void createContent(PdfContentByte cb, float x, float y, String text, int align) {

		cb.beginText();
		cb.setFontAndSize(bf, 8);
		cb.showTextAligned(align, text.trim(), x, y, 0);
		cb.endText();

	}

	private void initializeFonts() {

		try {
			bfBold = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPdfTitle() {
		return pdfTitle;
	}

	public void setPdfTitle(String pdfTitle) {
		this.pdfTitle = pdfTitle;
	}

	public String getPdfSubject() {
		return pdfSubject;
	}

	public void setPdfSubject(String pdfSubject) {
		this.pdfSubject = pdfSubject;
	}

	public String getPdfKeywords() {
		return pdfKeywords;
	}

	public void setPdfKeywords(String pdfKeywords) {
		this.pdfKeywords = pdfKeywords;
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            No args required.
	 */
	public static void main(String[] args) {
		LoginSession ls = new LoginSession(1, "jychtrvd@gmail.com", "mahesh", 2);
		EmailPdfVendor demo = new EmailPdfVendor("jychtrvd@gmail.com", "subject test", "content", "pdf title",
				"pdfSubject", "pdfKeywords");
		demo.email("rp2385881486", "3iaekHf8tr", ls);

		// CartModel cm=CartLogic.getCartModel("eu4608025362","3iaekHf8tr");
		// System.out.println(cm.getCompany_id()+" "+cm.getDescription()+"
		// "+cm.getSmrp());

	}
}