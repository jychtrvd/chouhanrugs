package com.model;


	

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="order_details")
public class OrderDetails {
	@Id
	@GeneratedValue
	private int id;
	
	@NotNull
	@Column(name = "orderId",nullable=false)
	private String orderId;
	
	@NotNull
	@Column(name = "status",nullable=false)
	private int status;

	@NotNull
	@Column(name = "uid",nullable=false)
	private int uid;
	
	@NotNull
	@Column(name = "confirm",nullable=false)
	private int confirm;
	
	
	@NotNull
	@Column(name = "visible",nullable=false)
	private int visible;
	
	
	@Column( name = "cdate", nullable = false ) 
    private Date cdate;
	
	@NotNull
	@Column(name = "total",nullable=false)
	private double total;
	
	@NotNull
	@Column(name = "other",nullable=false)
	private String other;
	
	@Column(name = "taxAndCharge",length=200, nullable = true)
	private String taxAndCharge;
	
	
	
	public  OrderDetails()
	{
		
	}

	public  OrderDetails(String orderId,int status,int uid,double total,String other,String taxAndCharge)
	{
		this.orderId=orderId;
		this.status=status;
		this.uid=uid;
		this.total=total;
		this.other=other;
		this.taxAndCharge=taxAndCharge;
		System.out.println("************  "+getCdate());
		this.cdate=getCdate();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	

	public int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public Date getCdate() {
		DateFormat inFormat3 = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			this.cdate=inFormat3.parse("2018-01-01 00:00:00");
			this.cdate=inFormat3.parse(OrderDetails.formatDateToString(date, "yyyy-MM-dd HH:mm:ss", "IST"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cdate;
	}

	public void setCdate(Date cdate) {
		
			this.cdate=cdate;
		
		
		//this.cdate = formatDateToString(date, "dd MMM yyyy hh:mm:ss a", "IST");
	}
	
	
	public int getConfirm() {
		return confirm;
	}

	public void setConfirm(int confirm) {
		this.confirm = confirm;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
	
	public String getTaxAndCharge() {
		return taxAndCharge;
	}

	public void setTaxAndCharge(String taxAndCharge) {
		this.taxAndCharge = taxAndCharge;
	}
	public static String formatDateToString(Date date, String format,
			String timeZone) {
		// null check
		if (date == null) return null;
		// create SimpleDateFormat object with input format
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		// default system timezone if passed null or empty
		if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
			timeZone = Calendar.getInstance().getTimeZone().getID();
		}
		// set timezone to SimpleDateFormat
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		// return Date in required format with timezone as String
		return sdf.format(date);
	}


	public static void main(String args[]){
		//Test formatDateToString method dd MMM yyyy HH:mm:ss   dd MMM yyyy hh:mm:ss a
				Date date = new Date();
				System.out.println("Default Date:"+date.toString());
				System.out.println("System Date: "+formatDateToString(date, "yyyy-MM-dd HH:mm:ss", null));
				System.out.println("System Date in PST: "+formatDateToString(date, "yyyy-MM-dd HH:mm:ss", "PST"));
				System.out.println("System Date in IST: "+formatDateToString(date, "yyyy-MM-dd HH:mm:ss", "IST"));
				System.out.println("System Date in GMT: "+formatDateToString(date, "yyyy-MM-dd HH:mm:ss", "GMT"));
	}

}
