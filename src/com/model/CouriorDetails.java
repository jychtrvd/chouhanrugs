package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="courior_details")
public class CouriorDetails {
	@Id
	@GeneratedValue
	@Column(name = "cid")
	private int cid;
	
	@Column(name = "couriorName", nullable = false)
	private String couriorName;

	@Column(name = "trackId", nullable = false)
	private String trackId;

	@Column(name = "orderId", nullable = false)
	private String orderId;
	
	@Column(name = "gid", nullable = true)
	private String gid;
	
	@Column(name = "status", nullable = false)
	private int status;
	
	@Column(name = "cdate", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	@Temporal(value = TemporalType.TIMESTAMP)
	@org.hibernate.annotations.Generated(value = GenerationTime.INSERT)
	private Date cdate;
	
	
	public CouriorDetails()
	{
		
	}


	public CouriorDetails(String name, String trackId2, String orderId2, String gid2) {
		
		this.couriorName=name;
		this.trackId=trackId2;
		this.orderId=orderId2;
		this.gid=gid2;
		
		
	}
	


	public int getCid() {
		return cid;
	}


	public void setCid(int cid) {
		this.cid = cid;
	}


	public String getCouriorName() {
		return couriorName;
	}


	public void setCouriorName(String couriorName) {
		this.couriorName = couriorName;
	}


	public String getTrackId() {
		return trackId;
	}


	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getGid() {
		return gid;
	}


	public void setGid(String gid) {
		this.gid = gid;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public Date getCdate() {
		return cdate;
	}


	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	
	

}
