package com.ccavenue;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.persistence.Column;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.CartModel;
import com.model.LoginSession;
import com.model.OrderDetails;
import com.model.ShippingModel;
import com.scope.CartSession;
import com.scope.PropertiesFile;
import com.scope.SessionUser;
import com.scope.TrippleDes;

//import sun.net.www.http.HttpClient;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author root
 */
public class JavaIntegrationKit {

	private Integer error;

	public boolean empty(String s) {
		if (s == null || s.trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}

	public String hashCal(String type, String str) {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append("0");
				}
				hexString.append(hex);
			}

		} catch (NoSuchAlgorithmException nsae) {
		}
		return hexString.toString();
	}

	protected Map<String, String> hashCalMethod(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LoginSession ls = (LoginSession) request.getSession().getAttribute("loginSession");

		response.setContentType("text/html;charset=UTF-8");
//		String salt = "e5iIg1jwi8";
		String salt="";
		String action1 = "";
	//	String base_url ="";/* "https://test.payu.in"; */
		String base_url="";
		error = 0;
		String hashString = "";
		Enumeration paramNames = request.getParameterNames();
		Map<String, String> params = new HashMap<String, String>();
		Map<String, String> urlParams = new HashMap<String, String>();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			String paramValue = request.getParameter(paramName);
			params.put(paramName, paramValue);
			System.out.println("paramName : "+paramName+" paramValue:"+paramValue);
		}
		/* payumoney require parameter */
		HashMap<String, String> hm=new HashMap<>();
		hm.put("payu.salt", "");
		hm.put("payu.key", "");
		hm.put("payu.base_url", "");
		for (String key : PropertiesFile.readAttribute(hm).keySet()) {
//			System.out.println(key + ":" + hm.get(key));
			if(key.equals("payu.salt"))
				salt=hm.get(key);
			else if(key.equals("payu.key"))
				params.put("key", hm.get(key));
			else if(key.equals("payu.base_url"))
				base_url=hm.get(key);
				
				
		}
//		params.put("key", "rjQUPktU");
		params.put("hash_string", "");
		params.put("hash", "");
		
		
		
//		params.put("service_provider", "payu_paisa");

		/* customer session information */
		String[] name = ls.getName().split(" ");
		params.put("firstname", name[0]);
		params.put("email", ls.getEmail());
		params.put("lastname", name[name.length - 1]);

		/* customer --billing /-- shipping information */
		ShippingModel sm = (ShippingModel) request.getSession().getAttribute("shippingModel");
		params.put("phone", sm.getsMobile());
		params.put("address1", sm.getsAddress1());
		params.put("address2", sm.getsAddress2());
		params.put("city", sm.getsCity());
		params.put("state", sm.getsState());
		params.put("country", "india");
		params.put("zipcode", sm.getsPincode());
		
	//	params.put("amount", String.valueOf(OrderLogic.totalOrderAmt(request)));
		params.put("amount", String.valueOf(50));

		/* trsaction detail send */
		String scheme = request.getScheme() + "://";
		String serverName = request.getServerName();
		String serverPort = (String.valueOf(request.getServerPort()) == "") ? "" : ":" + request.getServerPort();
		String contextPath = request.getContextPath();
		String url=scheme + serverName + serverPort + contextPath;
		
//		System.out.println("url :- "+url);

		params.put("curl", url+"/cancel");  /* Cancel URI */
		params.put("surl", url+"/reciept");  /*  Success URI */
		params.put("furl", url+"/cancel");  /* Failure URI */
		params.put("udf1", getOrderId());
		params.put("udf2", "udf2");
		params.put("udf3", "udf3");
		params.put("udf4", "udf4");
		params.put("udf5", "udf5");
		params.put("pg", "pg123");
		params.put("service_provider", "payu_paisa");
		
		String txnid = "";
		if (empty(params.get("txnid"))) {
			Random rand = new Random();
			String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
			txnid = rndm;
			params.remove("txnid");
			params.put("txnid", txnid);
			txnid = hashCal("SHA-256", rndm).substring(0, 20);
		} else {
			txnid = params.get("txnid");
		}
		

		params.put("txnid", txnid);
		System.out.println("txid  "+txnid);

		String txn = "abcd";
		String hash = "";
		String otherPostParamSeq = "phone|surl|furl|lastname|curl|address1|address2|city|state|country|zipcode|pg";
		String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		if (empty(params.get("hash")) && params.size() > 0) {
			if (empty(params.get("key")) || empty(txnid) || empty(params.get("amount"))
					|| empty(params.get("firstname")) || empty(params.get("email")) || empty(params.get("phone"))
					|| empty(params.get("productinfo")) || empty(params.get("surl")) || empty(params.get("furl"))
					|| empty(params.get("service_provider"))) {
				error = 1;
			} else {

				String[] hashVarSeq = hashSequence.split("\\|");
				for (String part : hashVarSeq) {
					if (part.equals("txnid")) {
						hashString = hashString + txnid;
						urlParams.put("txnid", txnid);
					} else {
						hashString = (empty(params.get(part))) ? hashString.concat("")
								: hashString.concat(params.get(part).trim());
						urlParams.put(part, empty(params.get(part)) ? "" : params.get(part).trim());
					}
					hashString = hashString.concat("|");
				}
				hashString = hashString.concat(salt);
				hash = hashCal("SHA-512", hashString);
				action1 = base_url.concat("/_payment");
				String[] otherPostParamVarSeq = otherPostParamSeq.split("\\|");
				for (String part : otherPostParamVarSeq) {
					urlParams.put(part, empty(params.get(part)) ? "" : params.get(part).trim());
				}

			}
		} else if (!empty(params.get("hash"))) {
			hash = params.get("hash");
			action1 = base_url.concat("/_payment");
		}

		urlParams.put("hash", hash);
		urlParams.put("action", action1);
		urlParams.put("hashString", hashString);
		return urlParams;
	}

	public static void trustSelfSignedSSL() {
		try {
			final SSLContext ctx = SSLContext.getInstance("TLS");
			final X509TrustManager tm = new X509TrustManager() {
				@Override
				public void checkClientTrusted(final X509Certificate[] xcs, final String string)
						throws CertificateException {
					// do nothing
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] xcs, final String string)
						throws CertificateException {
					// do nothing
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			ctx.init(null, new TrustManager[] { tm }, null);
			SSLContext.setDefault(ctx);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String getOrderId() {
		return TrippleDes.randomStringNumber(2, "str") + TrippleDes.randomStringNumber(10, "num");
	}
}