package com.ccavenue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.ccavenue.security.AesCryptUtil;
import com.google.gson.JsonObject;
import com.logic.CartLogic;
import com.logic.OrderLogic;
import com.model.LoginSession;
import com.model.ShippingModel;
import com.scope.DateConvert;
import com.scope.PropertiesFile;
import com.scope.SessionUser;
import com.scope.WriteFile;

/**
 * Servlet implementation class ccavenuePayment
 */
@WebServlet("/ccavenuePayment")
public class ccavenuePayment extends HttpServlet {
	private String merchantId = "";
	private String accessCode = "";
	private String workingKey = "";
	private String gatewayUrl="";
	private static final long serialVersionUID = 1L;
	public ccavenuePayment() {
		 super();
		// TODO Auto-generated constructor stub
		HashMap<String, String> hm=new HashMap<>();
		hm.put("ccavenue.url", "");
		hm.put("ccavenue.merchantId", "");
		hm.put("ccavenue.accessCode", "");
		hm.put("ccavenue.workingKey", "");
		for (String key : PropertiesFile.readAttribute(hm).keySet()) {
			if(key.equals("ccavenue.url"))
				gatewayUrl=hm.get(key);
			else if(key.equals("ccavenue.merchantId"))
				merchantId=hm.get(key);
			else if(key.equals("ccavenue.accessCode"))
				accessCode=hm.get(key);
			else if(key.equals("ccavenue.workingKey"))
				workingKey=hm.get(key);	
		}		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		// Put in the 32 Bit Working Key provided by CCAVENUES.
		String html = "";
		try {
			Enumeration enumeration = request.getParameterNames();
			String ccaRequest = "", pname = "", pvalue = "";
			while (enumeration.hasMoreElements()) {
				pname = "" + enumeration.nextElement();
				pvalue = request.getParameter(pname);
				ccaRequest = ccaRequest + pname + "=" + pvalue + "&";
			}
			AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			String encRequest = aesUtil.encrypt(ccaRequest);
			html = "<html> <body>"
					+ "<style type=\"text/css\">"
					+ ".loader {"
					+ "  margin-left: 550;"
					+ " margin-top: 200;"
					+ " border: 16px solid #f3f3f3; /* Light grey */"
					+ " border-top: 16px solid #3498db; /* Blue */"
					+ " border-radius: 50%;"
					+ " width: 120px;"
					+ " height: 120px;"
					+ " animation: spin 2s linear infinite;"
					+ " }"
					+ " @keyframes spin {"
					+ "  0% { transform: rotate(0deg); }"
					+ "  100% { transform: rotate(360deg); }"
					+ " }"
					+ " </style>"
					+ "<div class=\"container\">"
					+ "<div class=\"loader\"></div></div>";
			html += "<form id=\"nonseamless\" method=\"post\" name=\"redirect\" action=\""+gatewayUrl+"\"/>";
			html += "<input type=\"hidden\" id=\"encRequest\" name=\"encRequest\" value=\"" + encRequest + "\">";
			html += "<input type=\"hidden\" name=\"access_code\" id=\"access_code\" value=\"" + accessCode + "\">";
			html += "<script language='javascript'>document.redirect.submit();</script>";
			html += "</form>";
			html += "</body></html>";

		} catch (Exception e) {
			// TODO Auto-generated catch block
			html = e.toString();
		}
		writer.println(html);
	}

	public static void getResponseValue(HttpServletRequest request, String msg) {
		FileWriter fw = null;
		try {
			String root = request.getServletContext().getRealPath("/");
			File path = new File(root + "/ccav");
			if (!path.exists()) {
				path.mkdirs();
			}
			File file = new File(path + "/user.txt");
			if (!file.exists())
				file.createNewFile(); // the true will append the new data

			fw = new FileWriter(file, true);
			fw.write(msg);// appends the string to the file

		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg += "\n" + e.toString();
			try {
				fw.write(msg);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} // appends the string to the file
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static JSONObject getResponse(HttpServletRequest request) {
		boolean bol=true;
		JSONObject jobj=new JSONObject();  
		String str="\n\n\n\n\n*****ccavenupayment class *********\n\n\n\n\n\n\n\n";
		try {
			Enumeration<String> kayParams = request.getParameterNames();
			String workingKey = "53FBE866C0BCCFB92F417C564C00016B";		//32 Bit Alphanumeric Working Key should be entered here so that data can be decrypted.
			String encResp= request.getParameter("encResp");
			AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
			String decResp = aesUtil.decrypt(encResp);
			StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
			String pair=null, pname=null, pvalue=null;
			while (tokenizer.hasMoreTokens()) {
				pair = (String)tokenizer.nextToken();
				if(pair!=null) {
					StringTokenizer strTok=new StringTokenizer(pair, "=");
					pname=""; pvalue="";
					if(strTok.hasMoreTokens()) {
						pname=(String)strTok.nextToken();
						if(strTok.hasMoreTokens())
							pvalue=(String)strTok.nextToken();
						jobj = JsonObjectCcavenue.jobjData(pname, pvalue, jobj);
						
						str+=pname+"  ----- "+pvalue+"\n";
					}
				}
			}
			
			jobj.put("payment_mode", "cavenue");
			SessionUser su=new SessionUser();
			if (su.getSession(request, "loginSession")) {
				LoginSession ls = (LoginSession) request.getSession().getAttribute("loginSession");
				jobj.put("firstname", ls.getName());
				jobj.put("lastname", ls.getName());
			}
			else{
				jobj.put("firstname", "");
				jobj.put("lastname", "");
			}
				
		//	LoginSession ls = (LoginSession) request.getSession().getAttribute("loginSession");
			String txStatus = jobj.get("order_status").toString();
			
			

			WriteFile.writeFileWithMsg(request, jobj.toString()+"\n\n\n\n");
			
			
			WriteFile.writeFileWithMsg(request, "UPDATE payment_details SET gateWayCode='" + jobj.toString() + "', status=" + 1
					+ " WHERE orderId='" + jobj.get("order_id").toString() + "' and txid='" + jobj.get("merchant_param1").toString()+"'"+"\n\n\n\n");
			if (txStatus.equals("Success") | txStatus.equals("\"Success\"")) {
				OrderLogic.trasactionUpdate(jobj.get("order_id").toString(), jobj.get("merchant_param1").toString(),
						jobj.toString(), 1);
			} else
				OrderLogic.trasactionUpdate(jobj.get("order_id").toString(), jobj.get("merchant_param1").toString(),
						jobj.toString(), 2);
			request.getSession().setAttribute("order_no", jobj.get("order_id").toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			getResponseValue(request,e.toString());
		}
		return jobj;
	}
public static void main(String[] args) throws JSONException {
	String jsonString="{\"order_id\":\"hf6472179632\",\"lastname\":\"Mahesh Singh\"}";
	JSONObject obj=new JSONObject();    
	  obj.put("name","sonoo");    
	  obj.put("age",new Integer(27));    
	  obj.put("salary",new Double(600000));    
	   System.out.print(obj.get("name")); 
}

}
