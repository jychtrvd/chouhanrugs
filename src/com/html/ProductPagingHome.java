package com.html;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.derby.tools.sysinfo;

import com.logic.ProductLogic;
import com.nonModel.SlideHome;
import com.scope.PropertiesFile;

public class ProductPagingHome {

	public static String getProductShow(List<SlideHome> lsh, HttpServletRequest request) {
		String str = "";
		int i = 0;
		HashMap<String, String> hm = new HashMap<>();
		hm.put("currency.dollar", "");
		hm = PropertiesFile.readAttribute(hm);
		for (SlideHome sh : lsh) {
			i++;
			System.out.println(" Title= " + sh.getTitle());
			String[] arrOfStr = sh.getUrl().split(",");
			sh.getColor();
			if (i == 1)
				str += "<div class=\"pink\">";

			str += "<div class=\"col-md-3 box-in-at\">";
			str += "<div class=\"grid_box portfolio-wrapper\">";
			str += "<a href=\"" + request.getContextPath() + "/single/"+sh.getTitle().replace(" ", "-")+"/pk/" + sh.getPkey() + "\"> <img src=\""
					+ request.getContextPath() + "/img/fixed/" + arrOfStr[0]
					+ "\" class=\"img-responsive\" alt=\"\">";
			str += "<div class=\"zoom-icon\">";
			str += "<ul class=\"in-by\">";
			str += "<li><h5>sizes:</h5></li>";
			str += "<li><span>" + sh.getSize() + "</span></li>";
			str += "</ul>";
			str += "<ul class=\"in-by-color\">";
			str += "<li><h5>" + sh.getTitle() + "</h5></li>";
			str += "</ul>";
			str += "</div> </a> ";
			str += "</div>";
			str += "<!---->";
			str += "<div class=\"grid_1 simpleCart_shelfItem\">";
			str += "<a href=\"#\" class=\"cup item_add\"><span class=\"item_price\"><label>"+hm.get("currency.dollar")+"&nbsp;</label>"
					+ sh.getSmrp() + "<i> </i> </span></a>";
			str += "</div>";
			str += "<!---->";
			str += "</div>";
			if (i == 4) {
				str += "<div class=\"clearfix\"> </div></div>";
				i = 0;
			}

		}
		return str;
	}

	public static void main(String args[]) {

	}

}
