package com.logic;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.apache.log4j.Logger;

import com.cart.PaymentDetails;
import com.nonModel.SlideHome;
import com.scope.AbstractClass;
public class OrderLogic {
	static final Logger logger = Logger.getLogger(OrderLogic.class);
	public static boolean trasactionUpdate(String orderId, String trasactionid, String json, int con) {
		boolean bol = false;
		try {
			Transaction tx = null;
			Session session = null;
			PaymentDetails pd;
			try {
				pd = new PaymentDetails();
				session = com.model.HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();
				String query = "UPDATE payment_details SET gateWayCode='" + json + "', status=" + con+", visible=" + con
						+ " WHERE orderId='" + orderId + "' and txid='" + trasactionid+"'";
				System.out.println("query");
				bol = session.createSQLQuery(query).executeUpdate() > 0 ? true : false;
				session.getTransaction().commit();
			} catch (Exception e) {
				session.getTransaction().rollback();
			} finally {
				session.close();
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			// System.out.println(e.toString());
		}
		return bol;
	}
	public static SlideHome getSummery(boolean bol) {
		List<SlideHome> lsh = new java.util.ArrayList();
		SlideHome sh = new SlideHome();
		String query1 = "SELECT count(*) FROM order_details";
		String query2 = "SELECT count(*) FROM order_details where visible=0";
		String query3 = "SELECT count(*) FROM order_details where visible=1";
		String query4 = "SELECT count(*) FROM order_details where visible=-1 and status=-1";
		
		String query5 = "SELECT count(*) FROM payment_details";
		String query6 = "SELECT count(*) FROM payment_details where visible=0";
		String query7 = "SELECT count(*) FROM payment_details where visible=1";
		String query8 = "SELECT count(*) FROM payment_details where visible=-1";
		
		String query9  = "SELECT count(*) FROM product_detail";
		String query10 = "SELECT count(*) FROM product_detail where visible=0";
		String query11 = "SELECT count(*) FROM product_detail where visible=1";
		String query12 = "SELECT count(*) FROM product_detail where visible=-1";
		
		Session session=null;
		try {
			
			sh.setTotalOrder(AbstractClass.row_Count(query1,bol,session));
			sh.setPendingOrder(AbstractClass.row_Count(query2,bol,session));
			sh.setSuccessfullOrder(AbstractClass.row_Count(query3,bol,session));
			sh.setCancelOrder(AbstractClass.row_Count(query4,bol,session));
			
			sh.setTotalPayments(AbstractClass.row_Count(query5,bol,session));
			sh.setPendingPayments(AbstractClass.row_Count(query6,bol,session));
			sh.setSuccessFullPayments(AbstractClass.row_Count(query7,bol,session));
			sh.setCancelPayments(AbstractClass.row_Count(query8,bol,session));
			
			sh.setTotalProducts(AbstractClass.row_Count(query9,bol,session));
			sh.setPendingProductApproval(AbstractClass.row_Count(query10,bol,session));
			sh.setAvailableProducts(AbstractClass.row_Count(query11,bol,session));
			sh.setDisableProducts(AbstractClass.row_Count(query12,bol,session));
			
		} catch (Exception e) {
			logger.error("ProductLogic error(176)getProductHome  " + e.toString());
		}
		return sh;
	}
}
