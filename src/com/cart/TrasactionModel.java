package com.cart;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.logic.PaymentLogic;

public class TrasactionModel {
	public TrasactionModel() {
		// TODO Auto-generated constructor stub
	}

	public static JSONObject saveData(HttpServletRequest request) {
		Enumeration<String> kayParams = request.getParameterNames();
		JSONObject jobj = new JSONObject();
		try {
			while (kayParams.hasMoreElements()) {
				String key = (String) kayParams.nextElement();
				String value = request.getParameter(key).toString();
				jobj = jobjData(key, value, jobj);
				// result += key + "=" + request.getParameter(key) +
				// (kayParams.hasMoreElements() ? "," : "");
			}
			jobj.put("payment_mode", "payumoney");
			String txStatus = jobj.get("status").toString();
			System.out.println("txStatus:- " + txStatus);
			if (txStatus.equals("success") | txStatus.equals("\"success\"")) {
				PaymentLogic.trasactionUpdate(jobj.get("udf1").toString(), jobj.get("txnid").toString(), jobj.toString(), 1);
			} else
				PaymentLogic.trasactionUpdate(jobj.get("udf1").toString(), jobj.get("txnid").toString(), jobj.toString(), 2);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jobj;
	}

	private static JSONObject jobjData(String key, String value, JSONObject jobj) {
		if (!value.isEmpty()) {
			try {
				if (key.equals("mihpayid"))
					jobj.put(key, value);
				if (key.equals("status"))
					jobj.put(key, value);
				else if (key.equals("mode"))
					jobj.put(key, value);
				else if (key.equals("unmappedstatus"))
					jobj.put(key, value);
				else if (key.equals("txnid"))
					jobj.put(key, value);
				else if (key.equals("amount"))
					jobj.put(key, value);
				else if (key.equals("addedon"))
					jobj.put(key, value);
				else if (key.equals("email"))
					jobj.put(key, value);
				else if (key.equals("phone"))
					jobj.put(key, value);
				else if (key.startsWith("udf"))
					jobj.put(key, value);
				else if (key.startsWith("field"))
					jobj.put(key, value);
				else if (key.equals("PG_TYPE"))
					jobj.put(key, value);
				else if (key.equals("bank_ref_num"))
					jobj.put(key, value);
				else if (key.equals("bankcode"))
					jobj.put(key, value);
				else if (key.equals("bankcode"))
					jobj.put(key, value);
				else if (key.equals("error_Message"))
					jobj.put(key, value);
				else if (key.equals("cardnum"))
					jobj.put(key, value);
				else if (key.equals("cardhash"))
					jobj.put(key, value);
//			else if (key.equals("amount_split"))
//				jobj.addProperty(key, value);
				else if (key.equals("net_amount_debit"))
					jobj.put(key, value);
				else if (key.equals("payuMoneyId"))
					jobj.put(key, value);
				else if (key.equals("discount"))
					jobj.put(key, value);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return jobj;
	}

	private static int getFirstNumericDigit(String num) {
		Matcher matcher = Pattern.compile("\\d+").matcher(num);
		matcher.find();
		try {
			return Integer.valueOf(matcher.group());
		} catch (IllegalStateException e) {
		}
		return 0;

	}

	public static void main(String args[]) {
		String s = "";

		System.out.println(s.isEmpty());

	}
}
