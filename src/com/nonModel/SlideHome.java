package com.nonModel;

public class SlideHome {
	private String url;
	private String title;
	private String pkey;
	private String mrp;
	private String smrp;
	private String size;
	private String description;
	private String color;
	private int visible;
	private int count;
	
	
	private int totalOrder;
	private int pendingOrder;
	private int successfullOrder;
	private int cancelOrder;
	
	private int totalPayments;
	private int pendingPayments;
	private int successFullPayments;
	private int cancelPayments;
	
	private int totalProducts;
	private int pendingProductApproval;
	private int availableProducts;
	private int disableProducts;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPkey() {
		return pkey;
	}
	public void setPkey(String pkey) {
		this.pkey = pkey;
	}
	public String getMrp() {
		return mrp;
	}
	public void setMrp(String mrp) {
		this.mrp = mrp;
	}
	
	public String getSmrp() {
		return smrp;
	}
	public void setSmrp(String smrp) {
		this.smrp = smrp;
	}
	
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public int getVisible() {
		return visible;
	}
	public void setVisible(int visible) {
		this.visible = visible;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "mrp: "+mrp+" title:-"+title+"  pkey:-"+pkey+"  url"+url+" size:- "+size+" totalOrder:- "+totalOrder+" totalProducts:- "+totalProducts+" totalPayments:- "+totalPayments;
	}
	public int getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(int totalOrder) {
		this.totalOrder = totalOrder;
	}
	public int getPendingOrder() {
		return pendingOrder;
	}
	public void setPendingOrder(int pendingOrder) {
		this.pendingOrder = pendingOrder;
	}
	public int getSuccessfullOrder() {
		return successfullOrder;
	}
	public void setSuccessfullOrder(int successfullOrder) {
		this.successfullOrder = successfullOrder;
	}
	public int getCancelOrder() {
		return cancelOrder;
	}
	public void setCancelOrder(int cancelOrder) {
		this.cancelOrder = cancelOrder;
	}
	public int getTotalPayments() {
		return totalPayments;
	}
	public void setTotalPayments(int totalPayments) {
		this.totalPayments = totalPayments;
	}
	public int getPendingPayments() {
		return pendingPayments;
	}
	public void setPendingPayments(int pendingPayments) {
		this.pendingPayments = pendingPayments;
	}
	public int getSuccessFullPayments() {
		return successFullPayments;
	}
	public void setSuccessFullPayments(int successFullPayments) {
		this.successFullPayments = successFullPayments;
	}
	public int getCancelPayments() {
		return cancelPayments;
	}
	public void setCancelPayments(int cancelPayments) {
		this.cancelPayments = cancelPayments;
	}
	public int getTotalProducts() {
		return totalProducts;
	}
	public void setTotalProducts(int totalProducts) {
		this.totalProducts = totalProducts;
	}
	public int getPendingProductApproval() {
		return pendingProductApproval;
	}
	public void setPendingProductApproval(int pendingProductApproval) {
		this.pendingProductApproval = pendingProductApproval;
	}
	public int getAvailableProducts() {
		return availableProducts;
	}
	public void setAvailableProducts(int availableProducts) {
		this.availableProducts = availableProducts;
	}
	public int getDisableProducts() {
		return disableProducts;
	}
	public void setDisableProducts(int disableProducts) {
		this.disableProducts = disableProducts;
	}
	
	
	

}

